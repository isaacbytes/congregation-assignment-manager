// config/db.js


var mongoose = require('mongoose');
var gracefulShutdown;
//var dbURI = 'mongodb://localhost/congAssignmentBuilderDB';
var dbURI = 'mongodb://localhost/congAssignmentBuilderDB';
var dbTestURI = 'mongodb://localhost/congAssignmentTestEnv';

// environment variable in Heroku 'production'; undefined in local machine
// set MongoLab URI (set in Heroku settings)
if (process.env.NODE_ENV === 'production') {
  dbURI = process.env.MONGOLAB_URI;
}


// connect to DB
mongoose.connect(dbURI);

// MONGOOSE event listeners
mongoose.connection.on('connected', function() {
  console.log('Mongoose connected to: ' + dbURI);
});

mongoose.connection.on('error', function(error) {
  console.log('Mongoose connection error: ' + error);
});

mongoose.connection.on('disconnected', function() {
  console.log('Mongoose disconnected');
});


// function for closing Mongoose connection
gracefulShutdown = function(msg, callback) {
  mongoose.connection.close(function() {
    console.log('Mongoose disconnected through: ' + msg);
    callback();
  })
};

// APPLICTION LISTENERS
// event for NODEMON restarts
process.once('SIGUSR2', function() {
  gracefulShutdown('nodemon restart', function() {
    process.kill(process.pid, 'SIGUSR2');
  });
});

// event for application termination
process.once('SIGINT', function() {
  gracefulShutdown('app termination', function() {
    process.exit(0);
  });
});

// event for Heroku app termination
process.once('SIGTERM', function() {
  gracefulShutdown('Heroku app shutdown', function() {
    process.exit(0);
  });
});


