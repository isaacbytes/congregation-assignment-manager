builderApp.directive('brotherDetailsForm', function() {
  return {
    
    scope: {
      brother: '=',
      prvlgModel: '=',
      formAction: '&',
      childFormCtrl: '='
    },
    restrict: 'AE',
    templateUrl: '/js/components/directives-html/brotherdetailsform.html',
    link: function(scope, elem, attr) {
      scope.childFormCtrl = scope.brotherDetails;
    }
    
  }
});


builderApp.directive('brotherDetailsPreview', function() {
  return {
    
    scope: {
      brother: '=',
      prvlgModel: '='
    },
    restrict: 'AE',
    templateUrl: '/js/components/directives-html/brotherdetailspreview.html'
    
  }
});


builderApp.directive('mdwkMtgForm', function() {
  return {
    
    templateUrl: '/js/components/directives-html/mdwkMtgForm.html',
    restrict: 'AE',
    scope: {
      weekData: '=', 
      brothers: '=', // for suggestions/typeahead (for future fix: should really only be one-way)
      weekIndex: '@',
      addLivingAsTk: '&',
      removeLivingAsTk: '&',
      addAssigneeId: '&',
      regVisitTransform: '&',
      coVisitTransform: '&'
    }
    
  }
});


builderApp.directive('mdwkMtgTreasuresForm', function() {
  return {
   
    templateUrl: '/js/components/directives-html/mdwkMtgTreasuresForm.html',
    restrict: 'AE'
    
  }
});


builderApp.directive('mdwkMtgApplyYourselfForm', function() {
  return {
    
    templateUrl: '/js/components/directives-html/mdwkMtgApplyYourselfForm.html',
    restrict: 'AE'
    
  }
});


builderApp.directive('mdwkMtgLivingAsForm', function() {
  return {
    templateUrl: '/js/components/directives-html/mdwkMtgLivingAsForm.html',
    restrict: 'AE'
  }
});


builderApp.directive('mdwkMtgDynamicAssignee', function() {
  
  return {
    templateUrl: '/js/components/directives-html/mdwkMtgDynamic/assignee.html',
    restrict: 'E',
    scope: {
      asgmtAssigneeModel: '=',
      assigneeList: '<', // one-way binding, array of names  
      assigneeEditable: '@'
    },
    link: function(scope, elem, attrs) {
      var assigneeTextElem,
          assigneeInputElem;
        
      
      // convert raw directive elems to angular jqLite elems
      assigneeTextElem = angular.element(elem[0].firstChild);
      assigneeInputElem = angular.element(assigneeTextElem[0].nextElementSibling);
      
      // set css on elems
      assigneeTextElem.css({
        cursor: 'pointer',
        color: '#337ab7',
        paddingTop: '0.2em',
        paddingBottom: '0.2em'
      });
      
      assigneeInputElem.css({
        width: 'auto',
        paddingTop: '0.2em',
        paddingBottom: '0.2em'
      });
      
      // set event handlers
      assigneeTextElem.on('click', function(e) {
        assigneeInputElem.focus();
      });
      
    },
    
    controller: function($scope) {      
      $scope.editMode = false; // 'view' or 'edit'
      
            
      $scope.returnEditable = function() {
        if ($scope.assigneeEditable === 'false') {
          return false;
        } else {
          return true;
        }
      };
      
      
      $scope.editModeOn = function($event) {
        console.log($event);
        $scope.editMode = true;
      };
      
      
      $scope.doneEditingHandler = function(e) {
        // if 'enter' key is pressed
        if (e.type === 'keypress' && e.keyCode === 13) {
          $scope.editMode = false;
          return;
        }
        
        // if input is blurred
        if (e.type === 'blur') {
          $scope.editMode = false;
          return;
        }
      };
      
      
    }
  }
  
});




builderApp.directive('mdwkMtgDynamicAsgmtTitle', function() {
  
  return {
    templateUrl: '/js/components/directives-html/mdwkMtgDynamic/asgmtTitle.html',
    restrict: 'E',
    scope: {
      asgmtTitleModel: '=', // two-way bound title
      asgmtTitleList: '<', // one-way binding, array of suggested titles  
      asgmtTitleEditable: '@'
    },
    
    link: function(scope, elem, attrs) {
      var asgmtTitleTextElem,
          asgmtTitleInputElem;
      
      // convert raw directive elems to angular jqLite elems
      asgmtTitleTextElem = angular.element(elem[0].firstChild);
      console.log(asgmtTitleTextElem);
      
      asgmtTitleInputElem = angular.element(asgmtTitleTextElem[0].nextElementSibling);
      console.log(asgmtTitleInputElem);
      
      // set css on elems
      asgmtTitleTextElem.css({
        cursor: 'pointer',
        color: '#337ab7',
        paddingTop: '0.2em',
        paddingBottom: '0.2em'
      });
      
      asgmtTitleInputElem.css({
        width: 'auto',
        paddingTop: '0.2em',
        paddingBottom: '0.2em'
      })
      
      // set event handlers
      asgmtTitleTextElem.on('click', function(e) {
        asgmtTitleInputElem.focus();
      });
      
    },
    
    controller: function($scope) {      
      $scope.editMode = false; // 'view' or 'edit'
      
      $scope.editModeOn = function($event) {
        console.log($event);
        $scope.editMode = true;
      };
      
      
      $scope.returnEditable = function() {
        if ($scope.asgmtTitleEditable === 'false') {
          return false;
        } else {
          return true;
        }
      };
      
      
      $scope.doneEditingHandler = function(e) {
        // if 'enter' key is pressed
        if (e.type === 'keypress' && e.keyCode === 13) {
          $scope.editMode = false;
          return;
        }
        
        // if input is blurred
        if (e.type === 'blur') {
          $scope.editMode = false;
          return;
        }
      };
      
      
    }
  }
  
});


builderApp.directive('mdwkMtgDynamicDuration', function() {
  
  return {
    templateUrl: '/js/components/directives-html/mdwkMtgDynamic/duration.html',
    restrict: 'E',
    scope: {
      asgmtDurationModel: '=', // two-way bound duration
      asgmtMaxDigits: '@', // max digits to pass
      asgmtDurationEditable: '@' // 'true' or 'false'
    },
    
    link: function(scope, elem, attrs) {
      var asgmtDurationTextElem,
          asgmtDurationInputElem;
      
      // convert raw directive elems to angular jqLite elems
      asgmtDurationTextElem = angular.element(elem[0].firstChild);
      
      asgmtDurationInputElem = angular.element(asgmtDurationTextElem[0].nextElementSibling);
      
      // set css on elems
      asgmtDurationTextElem.css({
        cursor: 'pointer',
        color: '#337ab7',
        paddingTop: '0.2em',
        paddingBottom: '0.2em'
      });
      
      asgmtDurationInputElem.css({
        width: 'auto',
        paddingTop: '0.2em',
        paddingBottom: '0.2em'
      })
      
      // set event handlers
      asgmtDurationTextElem.on('click', function(e) {
        asgmtDurationInputElem.focus();
      });
      
    },
    
    controller: function($scope) {      
      $scope.editMode = false; // 'view' or 'edit'
      
      $scope.editModeOn = function($event) {
        console.log($event);
        $scope.editMode = true;
      };
      
      $scope.asgmtMaxDigits = $scope.asgmtMaxDigits || 3;
      
      $scope.returnEditable = function() {
        if ($scope.asgmtDurationEditable === 'false') {
          return false;
        } else {
          return true;
        }
      };
      
      
      $scope.doneEditingHandler = function(e) {
        // if 'enter' key is pressed
        if (e.type === 'keypress' && e.keyCode === 13) {
          $scope.editMode = false;
          return;
        }
        
        // if input is blurred
        if (e.type === 'blur') {
          $scope.editMode = false;
          return;
        }
      };
      
      
    }
  }
  
});


//mdwkMtgDynamicAsgmtGroup
//mdwkMtgDynamicMtgSection
builderApp.directive('mdwkMtgDynamicAsgmtGroup', function() {
  
  return {
    templateUrl: '/js/components/directives-html/mdwkMtgDynamic/asgmtGroup.html',
    restrict: 'E',
    scope: {
      asgmtDurationModel: '=',
      asgmtDurationEditable: '@',
      
      asgmtTitleModel: '=',
      asgmtTitleList: '<',
      asgmtTitleEditable: '@',
      
      asgmtAssigneeModel: '=',
      asgmtAssigneeList: '<',
      asgmtAssigneeEditable: '@'
      
    },
    
    link: function(scope, elem, attrs) {
      
      console.log(scope);
      
    },
    
    controller: function($scope) {      
//      $scope.asgmtDurationModel = 8;
//      $scope.asgmtDurationEditable = 'false';
//      
//      $scope.asgmtTitleModel = 'Busquemos perlas escondidas';
//      $scope.asgmtTitleList = ['Busquemos perlas escondidas', 'Discurso'];
//      $scope.asgmtTitleEditable = 'true';
//      
//      $scope.asgmtAssigneeModel = 'Gustavo Rodriguez',
//      $scope.asgmtAssigneeList = ['Alan Martinez', 'Gustavo Rodriguez', 'Isaac Farciert'];
//      $scope.asgmtAssigneeEditable = 'false';
      
      
    }
  }
  
});

