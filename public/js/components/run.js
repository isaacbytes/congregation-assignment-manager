angular
  .module('builderApp')
  .run(['AuthenticationService', 'GoogleGeoCodeService', function(AuthenticationService, GoogleGeoCodeService){
    
    if (AuthenticationService.isLoggedIn()) {
      
      console.log('User is logged in. Session info:');
      console.log(AuthenticationService.getSessionInfo());
      console.log('Resumimg automatic token renewal (ATR)');
      AuthenticationService.resumeATR();
      
    }
    
    else {
      console.log('No user logged in.');
    }
    
    
  }]);