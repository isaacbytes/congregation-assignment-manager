// ROUTES

angular.module('builderApp')
.config(function ($routeProvider) {
  
  $routeProvider
  .when('/', {
    
    templateUrl: 'js/components/brothers/views/brothers.html',
    controller: 'brothersController'
    
  })
  
  // BROTHERS
  .when('/brothers', {
    
    templateUrl: 'js/components/brothers/views/brothers.html',
    controller: 'brothersController'
    
  })
  
  .when('/brothers/edit/:id', {
    
    templateUrl : 'js/components/brothers/views/brother-edit.html',
    controller : 'editBrotherController'
    
  })
  
  .when('/brothers/new/:name?', {
  
    templateUrl: 'js/components/brothers/views/brother-new.html',
    controller : 'newBrotherController'
    
  })
  
  
  // MIDWEEK MEETING
  .when('/mdwkMtg', {
    
    templateUrl: 'js/components/mdwkMtg/views/main.html',
    controller: 'mdwkMtgController'
    
  })
  
  .when('/mdwkMtg/edit/:mmyyyy', {
    
    templateUrl: 'js/components/mdwkMtg/views/edit.html',
    controller: 'newMdwkMtgController'
    
  })
  
  .when('/mdwkMtg/view/:mmyyyy', {
    
    templateUrl: 'js/components/mdwkMtg/views/view.html',
    controller: 'viewMdwkMtgController'
    
  })
  
  
  // ROTATIONAL ASSIGNMENTS HELPER
  .when('/rotationals', {
    
    templateUrl: 'js/components/rotationals/views/main.html',
    controller: 'rotationalsMainController'
    
  })
  
  
  // SERVICE GROUPS 
  .when('/serviceGroups', {
    
    templateUrl: 'js/components/serviceGroups/views/main.html',
    controller: 'serviceGroupsMainController'
    
  })
  
  
  // AUTH
  .when('/login/', {
    
    templateUrl: 'js/components/auth/views/login-view.html',
    controller: 'loginCtrl'
    
  })

  
  .when('/login/:destination*', {
    
    templateUrl: 'js/components/auth/views/login-view.html',
    controller: 'loginCtrl'
    
  })
  
  
  .when('/not-authorized', {
    
    templateUrl: 'js/components/auth/views/not-authorized.html',
    controller: 'notAuthorizedCtrl'
    
  })

  
  // MY ACCOUNT
  .when('/my-account/', {
    
    templateUrl: 'js/components/myAccount/views/home.html',
    controller: 'myAcctHome'
    
  });
  
});