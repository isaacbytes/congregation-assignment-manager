// CONFIG / INITIALIZATION FILES



builderApp.config(function($httpProvider, $locationProvider) {
  
  /* ************ HTTP CONFIG ************** */
  $httpProvider.interceptors.push(function($window, $q, $injector, $location) {

    return {
      // *** REQUEST INTERCEPTORS
      request: function(req) {
        var jwtToken = $window.localStorage['authToken'];
        
        // ONLY pass auth token to API calls
        if (req.url.indexOf('/api/') !== 0) {
          return req;
        }
        
        // do not pass auth token if signing in
        if (req.url === '/api/auth' && req.data.email && req.data.password) {
          return req;
        }
        
        // if JWT token exists in local storage, append to HTTP Headers
        else if (jwtToken) {
          req.headers.Authorization = 'Bearer ' + jwtToken;
          console.log('########## Appended token. Request: ');
          console.log(req);
          return req;
        }
        
        // if nothing, return regular headers
        return req;
      },
      
      
      
      // *** ERROR RESPONSE INTERCEPTORS
      responseError : function(rejection) {
        console.log('Error response interceptor fired!');
        
        var rawUrl,
            destinationUrl,
            parsedUrlPaths = [];
        
        // if not logging into home, parse destination url
        // ** When multiple requests made, catch the first one, and ignore
        // the rest. Do not bubble requests and catch their destinations
        // (i.e. from: '/login/login/login/login/destination'
        // to : '/destination'
        if ($location.url() !== '/') {
          // e.g. rawUrl = '/path1/path2/path3'
          rawUrl = $location.url();
          
          // split paths based on (/)
          parsedUrlPaths = rawUrl.split('/');
          
          // remove blank space from first '/'
          parsedUrlPaths.splice(0,1);
          
          // remove any duplicated login paths from multiple reqs
          parsedUrlPaths = _.filter(parsedUrlPaths, function(path) {return path !== 'login'});
          
          // reconstruct url path
          destinationUrl = '/' + parsedUrlPaths.join('/');

          console.log('Destination url set to: ' + destinationUrl);
          console.log('Parsed paths:' + parsedUrlPaths);
        }
        
        
      
        
        // 401 Unauthorized (not authenticated) Errors
        if (rejection.status === 401) {   
          console.log('Error identified as 401 Unauthorized, redirect to login (location-aware)');
          
          if (destinationUrl) {
            console.log('Destination url present, attach to URL: ' + destinationUrl);
            console.log('Redirecting...');
            
            $location.path('/login' + destinationUrl);
           
            console.log('********* Redirected. END OF INTERCEPTOR FN');  
            return rejection;
          }
          else {
            
            console.log('Destination url not present. Do not pass any destination args');
            console.log('Redirecting...');
            
            $location.path('/login');
            
            console.log('********* END OF INTERCEPTOR FN');  
            return rejection;
          }
          
        }
        
        // 403 Forbidden Errors
        if (rejection.status === 403) {
          
          console.log('Error identified as 403 Forbidden, redirect to forbidden page');
          
          $location.path('/not-authorized');
          return rejection;
        }
        
        
        console.log('returning rejection');
        console.log(rejection);
        
        return $q.reject(rejection);
      }
    }
  });
  
  
  
  /* ************ $locationProvider CONFIG ************** */
  
  
  
});