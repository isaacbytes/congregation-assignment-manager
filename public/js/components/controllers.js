// BROTHERS CONTROLLER
builderApp.controller('brothersController', ['$scope', '$location', '$http', '$uibModal', '$filter', 'brotherService', 'Sort',  function($scope, $location, $http, $uibModal, $filter, brotherService, Sort) {
  
  
  
  // CONFIG
  $scope.brFilters = {};
  $scope.prvlgsAll = ['mdwkcbsreader', 'mdwkstudent', 'mdwkprayer', 'sunchairman', 'sunwtreader', 'sound', 'platform', 'mics',  'attendant', 'pubwitness'];
  $scope.prvlgsFil = {};
  $scope.brFilters.role = ''; // initialize default state
  $scope.searchText = '';
  $scope.filterAccordion = false;
  $scope.showDetails = false;
  
  $scope.brothers = brotherService.query(function successCb() {
    $scope.brothers = Sort.roleAlphab($scope.brothers); // sort data
  }, function errCb(err) {
      console.log('GET brothers error! HTTP Response : ', err);
    }
  );
  

  // FUNCTIONS
  $scope.toggleFilterAccordion = function() {
    $scope.filterAccordion = !$scope.filterAccordion;  
  };
  
  $scope.editBrother = function(id) {
    $location.path('/brothers/edit/' + id);
  };
  
  $scope.toggleView = function() {
    $scope.showDetails = !$scope.showDetails;
  };
  
  $scope.removeFilter = function(key) {
    $scope.prvlgsFil[key] = false;
  };
  
  $scope.prvlgFilters = function(brother, index, array) {
    var meetsConditions = true;
    
    // for each of the selected privilege filters
    _.each($scope.prvlgsFil, function(val, key, index) {
      if (val === true) {
        
        // if brother has privilege, continue checking (state var remains true)
        if (brother.prvlgs.indexOf(key) > -1) {
          
        // brother does not have prvlg, change state var to false
        } else {
          meetsConditions = false;
          return false;
        } 
      }
    });
    
    // return state variable
    return meetsConditions;
  };
  
  // search filter predicate 
  $scope.searchInput = function(value) {
    var strippedSearch = $filter('removeAccents')($scope.searchText),
        strippedFullName = $filter('removeAccents')(value.firstName) + ' ' + $filter('removeAccents')(value.lastName);
    return strippedFullName.indexOf(strippedSearch) > - 1;
  };
  
}]);


// MODAL CONTROLLER
builderApp.controller('deleteBrotherModalCtrl',['$scope', '$uibModalInstance', 'brother', function($scope, $uibModalInstance, brother) {
  
  $scope.brother = brother;
  
  $scope.confirmDelete = function() {
    // resolve 'result' promise
    $uibModalInstance.close();
  };
  
  $scope.cancel = function() {
    // reject 'result' promise
    $uibModalInstance.dismiss('cancel');
  }
  
}]);



// EDIT BROTHER CONTROLLER
builderApp.controller('editBrotherController', ['$scope', '$http', '$routeParams', '$location', '$uibModal', 'brotherService',  function($scope, $http, $routeParams, $location, $uibModal, brotherService) {
  
  $scope.brotherID = $routeParams.id;
  $scope.masterBrother = {}; // from db
  $scope.brother = {}; // local model
  $scope.prvlgModel = {}; // prvlg model
  $scope.childFormCtrl = {}; // access properties for form validation
  
  
  // BOOTSTRAP: get brother data from api
  $scope.masterBrother = brotherService.get({userid : $scope.brotherID}, function successCb(res) {
    
    // create deep copy of api data for modification
    $scope.brother = angular.copy(res);
    
    // create local privilege model
    _.each($scope.brother.prvlgs, function(elem, index, list) {      
      $scope.prvlgModel[elem] = 'hasPrvlg';
    });
  }, function errCb(res) {
    console.log('Error retrieving from API!', res);
  });
  

  
  
  // PAGE FUNCTIONS
  $scope.cancelPage = function() {
    $location.path('/brothers');
  };
  
  
  $scope.deleteBrother = function() {
    var modalInstance = $uibModal.open({
      templateUrl: '/js/components/modals/deleteBrother.html',
      controller: 'deleteBrotherModalCtrl',
      resolve: {
        brother: function() {
          return {
            id: $scope.masterBrother._id,
            firstName: $scope.masterBrother.firstName,
            lastName: $scope.masterBrother.lastName
          };
        }
      }
    });
    
    modalInstance.result.then(function resolved() {
      
      // delete brother
      brotherService.delete({ userid : $scope.masterBrother._id }, function success() {
        
        // ** bug fix ** (when using search, the $index of the brother changes, look for index)
//        var updatedIndex = $scope.brothers.indexOf($scope.brothers.find(function(b){return b._id===id;}));
//        
//        if (updatedIndex > - 1) {
//          $scope.brothers.splice(updatedIndex, 1);
//        } else {
//          console.log('cannot find brother in array!');
//        }
        
        $location.path('/brothers');
        
      }, function error() {
        // cancel function
      });
      
    }, function rejected() {
      // cancel
    });
  };
  
  
  
  $scope.submitForm = function() {
    
    if ($scope.childFormCtrl.$invalid) {
      $scope.viewNotifications = "Por favor rellene todos los datos abajo.";
      return;
    } 
    else {
      // create req object
      var reqObj = {
        firstName     : $scope.brother.firstName,
        lastName      : $scope.brother.lastName,
        isPublisher   : $scope.brother.isPublisher,
        isBaptized    : $scope.brother.isBaptized,
        role          : $scope.brother.role,
        gender        : $scope.brother.gender,
        prvlgs        : []
      };
      
      
      _.each($scope.prvlgModel, function (hasOrNot, prvlg) {
        // attach prvlgs array
        if (hasOrNot === "hasPrvlg") {
          reqObj.prvlgs.push(prvlg);
        }
      });

      // send PUT request
      brotherService.update({userid : $scope.brotherID}, reqObj, function successCb(res) { 
        // redirect to brothers view
        $location.path('/brothers');

      }, function errCb(res) {
        // notify users of error
        $scope.viewNotifications = 'Hubo un error procesando la información. Por favor intente otra vez.';
      });
    }
  };
  
}]);


// NEW BROTHER CONTROLLER
builderApp.controller('newBrotherController', ['$scope', '$http', '$routeParams', '$location', 'brotherService',  function($scope, $http, $routeParams, $location, brotherService) {
  
  
  $scope.childFormCtrl = {};
  
  // default brother profile
  $scope.brother = {
    role : 'other',
    gender : 'm',
    isPublisher : true,
    isBaptized : false
  };
  
  // if preloaded name, add name to field
  if ($routeParams.name) {
    var names = $routeParams.name.split(' ');
    if (names.length > 1) {
      names.forEach(function(val, index, arr) {
        if (index !== (arr.length - 1)) {
          index === 0? $scope.brother.firstName = val : $scope.brother.firstName += ' ' + val;
        } else {
          $scope.brother.lastName = val;
        }
      });
    } else {
      $scope.brother.firstName = names[0];
    }
  }
  
  // default brother privilege
  $scope.prvlgModel = {
    // student in midweek meeting
    mdwkstudent : 'hasPrvlg'
  };
  
  // FUNCTIONS
  $scope.submitForm = function() {
    if ($scope.childFormCtrl.$invalid) {
      $scope.viewNotifications = "Por favor rellene todos los datos abajo."
      return;
    }
    else {
      // create req object
      var reqObj = {
        firstName     : $scope.brother.firstName,
        lastName      : $scope.brother.lastName,
        role          : $scope.brother.role,
        gender        : $scope.brother.gender,
        isPublisher   : $scope.brother.isPublisher,
        isBaptized    : $scope.brother.isBaptized,
        prvlgs        : []
      };
      _.each($scope.prvlgModel, function (hasOrNot, prvlg) {
        // attach prvlgs array
        if (hasOrNot === "hasPrvlg") {
          reqObj.prvlgs.push(prvlg);
        }
      });

      // create instance and send POST
      var newBrother = new brotherService(reqObj);
      newBrother.$save({}, function success(value, resHeaders) {
        // redirect to brothers view
        $location.path('/brothers');

      }, function err() {
        // notify users of error
        $scope.viewNotifications = 'Hubo un error procesando la información. Por favor intente otra vez.';

      });
    }
    
    
//    Resource.action([parameters], postData, [success], [error])
    
  };
  
  
}]);



// MIDWEEK MEETING CONTROLLER
builderApp.controller('mdwkMtgController', ['$scope', '$log', '$uibModal', 'mdwkMtgServicePerDay', 'MdwkMonthIsCompleteService', 'DatesUtil', function($scope, $log, $uibModal, mdwkMtgServicePerDay, MdwkMonthIsCompleteService, DatesUtil) {
  
  var numPreviousMonths = 5,
      numUpcomingMonths = 6,
      lowerMomentBound,
      upperMomentBound;
  

  $scope.todayMoment = DatesUtil.cloneTodayMoment();
  $scope.todayDate = $scope.todayMoment.toDate();
  $scope.yearMonths = moment.monthsShort();
  
  // should be 6 months before, 5 months after curDate
  $scope.displayMonths = [];
  
  // set up bounds of months display interface
  lowerMomentBound = moment(DatesUtil.cloneTodayMoment().subtract(numPreviousMonths, 'months')).startOf('month');
  upperMomentBound = moment(DatesUtil.cloneTodayMoment().add(numUpcomingMonths, 'months')).startOf('month');
  
  // Get today's midweek meeting data
  mdwkMtgServicePerDay.get({date: $scope.todayMoment.format('YYYY-MM-DD')}, function success(res) {
    $scope.todayData = res;
    console.log(res);
  }, function err(err) {
    $scope.notificationsPane.type = 'error';
    $scope.notificationsPane.message = err.status || 'Error.';
  });
  
  
  // fill up months overview array
  while (lowerMomentBound.isSameOrBefore(upperMomentBound, 'month')) {
    // push to display array
    $scope.displayMonths.push(lowerMomentBound.clone());
    
    // loop step
    lowerMomentBound.add(1, 'months');
  }
  
  // update completion status of each month
  for (var i = 0; i < $scope.displayMonths.length; i++) {
    $scope.displayMonths[i].query = MdwkMonthIsCompleteService.get({date: $scope.displayMonths[i].format('MM-YYYY')});
  }
  
  
  $scope.notificationsPane = {
    type : '',
    message : ''
  };
  
  $scope.yearOverviewCollapsed = false;
  
  $scope.toggleYearOverview = function() {
    $scope.yearOverviewCollapsed = !$scope.yearOverviewCollapsed;
  }
  
  
  

  
  
}]);


// EDIT MIDWEEK MEETING CONTROLLER
builderApp.controller('newMdwkMtgController', ['$scope', '$log', '$http', '$uibModal', '$routeParams', '$location', '$q', 'mdwkMtgServicePerDay', 'DatesUtil', 'brotherService', 'MdwkScaffolding', 'MdwkMtgMonthService', function($scope, $log, $http, $uibModal, $routeParams, $location, $q, mdwkMtgServicePerDay, DatesUtil, brotherService, MdwkScaffolding, MdwkMtgMonthService) {
  
  
  $scope.brothers = brotherService.query(function successCb(data) {
    
    // testing new directive
    $scope.brothersClone = _.map(angular.copy($scope.brothers), function(brotherObj) {
      return brotherObj.firstName + ' ' + brotherObj.lastName;
    });
    
  },
    function errCb(err) {
      console.log('GET brothers error! HTTP Response : ', err);
    }
  );
  
  $scope.testDuration = 4;
  $scope.testAssignee = '';
  $scope.testTitle = 'Discurso';
  $scope.testTitleList = ['Busquemos perlas escondidas', 'Discurso', 'Hola'];

  
  // container for mdwk meeting data 
  $scope.mdwkMtgData = [];
  
  // container for 'data' filled weeks from API
  $scope.mdwkPresentMoments = [];
  
  // create new Moment based on route params
  $scope.mdwkMtgMoment = moment($routeParams.mmyyyy, 'MM-YYYY');
   
  // grab native Date obj reference
  $scope.mdwkMtgDate = $scope.mdwkMtgMoment.toDate();
  
  // declare array to hold all 'weeks' in month (mondays)
  $scope.mondays = DatesUtil.getMondays($scope.mdwkMtgMoment.format('MM-YYYY'));
  
  // get month mtg data
  $scope.mdwkMtgData  = MdwkMtgMonthService.query({date: $scope.mdwkMtgMoment.format('MM-YYYY')}, handleApiResponse, handleApiResponse);
  
  // toggle CO Visit/CBS
  $scope.coVisitTransform = function(weekObj) {
    if (weekObj.livingAsCollection[weekObj.livingAsCollection.length-1].asgmtCode === 'TK') {
      return;
    }
    MdwkScaffolding.toggleCoVisit(weekObj);
  };
  
  $scope.regVisitTransform = function(weekObj) {
    if (weekObj.livingAsCollection[weekObj.livingAsCollection.length-1].asgmtCode === 'CBS') {
      return;
    }
    MdwkScaffolding.toggleCoVisit(weekObj);
  }
  
  
  function handleApiResponse() {
    $scope.mdwkMtgData.forEach(function(mtg, index, arr) {
      $scope.mdwkPresentMoments.push(moment(mtg.date));
    });
    $scope.mondays.forEach(function(monday, index, arr) {
      // is this monday in the data that came back from API?
      var hasMonday = _.find($scope.mdwkPresentMoments, function(mt) {
        return monday.isSame(mt, 'day');
      });
      
      if (hasMonday) {
        // do nothing, week is present  
      } else {
        // add scaffolded week
        $scope.mdwkMtgData.push(MdwkScaffolding.mtgScaffoldByDate(monday));
      }
    });
  }
  

  /* Functions */
  
  // add a talk field to Living As Christians array
  $scope.addLivingAsTk = function(wkIndex) {
    $scope.mdwkMtgData[wkIndex].livingAsCollection.splice(-1, 0, {
      asgmtCode : 'TK',
      asgmtTitle : '',
      duration : 10,
      assignee : '',
      removable : true
    });
  };
  
  
  // remove input field from Living As Christians array
  $scope.removeLivingAsTk = function(wkIndex, tkIndex) {
    console.log(wkIndex + ' ' + tkIndex);
    $scope.mdwkMtgData[wkIndex].livingAsCollection.splice(tkIndex, 1);
  };
  
  // POSTs entire month, week by week
  $scope.submitMtgs = function() {
    $scope.mdwkMtgData.forEach(function(mtg, index, arr) {
      
      // strip null values
      _.each(mtg, function(val, key, list) {
        if (val === null) {
          console.log('null value!');
          delete list[key];
        }
      });
      
      if (mtg.specialWk === 'assembly') {
        mtg.treasuresCollection = '';
        mtg.applyYourselfCollection = '';
        mtg.livingAsCollection = '';
      }
      
    });
    
    MdwkMtgMonthService.save(
      {date: $scope.mdwkMtgMoment.format('MM-YYYY')},
      $scope.mdwkMtgData,
      function success(data) {
        $location.path('/mdwkMtg/view/' + $scope.mdwkMtgMoment.format('MM-YYYY'));
      },
      function error(data) {
        console.log('error!');
        console.log(data);
      }
    );
    
  };
  
  
  // attaches the corresponding brotherID to the assignee model to be sent to API
  $scope.addAssigneeId = function(wkIndex, section, index, brother, secondaryIndex) {

    // non-section-specific data ('index' should be name of asgmt)
    if (section === 'meta') {
      path = $scope.mdwkMtgData[wkIndex][index];
    }
    
    else if (section === 'treasuresCollection') {
     path = $scope.mdwkMtgData[wkIndex].treasuresCollection[index];
    }
    
    else if (section === 'applyYourselfCollection') {
      if (wkIndex == 0) {
        path = $scope.mdwkMtgData[wkIndex].applyYourselfCollection[index];
      }
      // if multiple participants
      else {
        $scope.mdwkMtgData[wkIndex].applyYourselfCollection[index].assigneeID[secondaryIndex] = brother._id;
        return;
      }
    }
    else if (section === 'livingAsCollection') {
      // if multiple participants, assume non-numerical secondary index (e.g. 'reader'); assign and exit function
      if (secondaryIndex) {
        $scope.mdwkMtgData[wkIndex].livingAsCollection[index][secondaryIndex+'ID'] = brother._id;
        return;
      }
      else {
        path = $scope.mdwkMtgData[wkIndex].livingAsCollection[index];
      }
    }
    path.assigneeID = brother._id;
  };
  
  
}]);




// MONTH-VIEW MDWK MTG
builderApp.controller('viewMdwkMtgController', ['$scope', '$log', '$http', '$uibModal', '$routeParams', 'MdwkMtgMonthService', 'DatesUtil', function($scope, $log, $http, $uibModal, $routeParams, MdwkMtgMonthService, DatesUtil) {

  $scope.printPage = function() {
    window.print();
  };
  $scope.mtgStartTime = DatesUtil.cloneTodayMoment().hour(19).minute(30);
  $scope.error = false;
  $scope.mdwkMtgMoment = moment($routeParams.mmyyyy, 'MM-YYYY');
  $scope.mdwkMtgDate = $scope.mdwkMtgMoment.toDate();
  
  // get month data
  if (!$scope.mdwkMtgMoment.isValid()) {
    console.log('Invalid url! \n form: "MM-YYYY" ');
  }
  
  $scope.monthData = MdwkMtgMonthService.query({date: $scope.mdwkMtgMoment.format('MM-YYYY')}, function success(res){
    
  }, function err(res) {
    console.log('error!');
    $scope.error = res;
  });    
  
}]);



/* *********** ROTATIONAL DATA CONTROLLERS *********** */
builderApp.controller('rotationalsMainController', ['$scope', '$log', '$http', '$routeParams', 'RotationalColService', 'DatesUtil', function($scope, $log, $http, $routeParams, RotationalColService, DatesUtil) {
  
  $scope.mdwkMtgRotation = [];
  $scope.wkndMtgRotation = [];
  $scope.prvlgCols = RotationalColService.query(function success(res) {
    var prvlgData = res;
    // filter and separate mtg types
    for (var i = 0; i < prvlgData.length; i++) { 
      var curCol = prvlgData[i];
      
      if (curCol.mtgLoc === 'mdwk') {
        $scope.mdwkMtgRotation.push(curCol[i]);
      }
      
      else if (curCol.mtgLoc === 'wknd') {
        $scope.wkndMtgRotation.push(curCol[i]);
      }
      
      else {
        console.error('No mtg type defined in $scope for: ' + curCol);
      }
    }
  }); 
  
}]);




/* *********** SERVICE GROUPS CONTROLLERS *********** */
builderApp.controller('serviceGroupsMainController', ['$scope', '$log', '$http', '$routeParams', 'ServiceGroupsService', 'DatesUtil', function($scope, $log, $http, $routeParams, ServiceGroupsService, DatesUtil) {
  
  
  $scope.serviceGroups = ServiceGroupsService.query();
  
  
  
}]);




/* *********** AUTH CONTROLLERS *********** */
builderApp.controller('loginCtrl', ['$scope', '$location', '$routeParams', 'AuthenticationService', function($scope, $location, $routeParams, Auth){
  
  // intercepted a destination? Preserve.
  var destinationPath = $routeParams.destination || '/';
  
  // Landed on page by mistake? Check if user is already logged in.
  if (Auth.isLoggedIn()) {
    $location.path('/');
  }
  
  $scope.statusUpdate = '';
 
  
  // Submit button
  $scope.submitCredentials = function() {
    Auth.logInUser($scope.user).then(function(httpResponse){
      // credentials success, redirect to destination
      console.log('success!');
      $location.path(destinationPath);
      
    }, function(httpError) {
      // check status code
      
      // Incorrect credentials
      if (httpError.status === 401) {
        $scope.statusUpdate = 'Esas credenciales no son válidas.'
      }
      
      // Not authorized to view resource
      if (httpError.status === 403) {
        $scope.statusUpdate = 'Su cuenta no está autorizada para ver estos datos.'
      }
      
      console.log(httpError);
    });
  };
  
  
}]);


builderApp.controller('notAuthorizedCtrl', ['$scope', '$location', '$routeParams', 'AuthenticationService', function($scope, $location, $routeParams, Auth) {
  
  // 
  
  
}]);

builderApp.controller('myAcctHome', ['$scope', '$location', '$routeParams', 'AuthenticationService', function($scope, $location, $routeParams, Auth) {
  
  
  
  
  
  
}]);

  
  
  