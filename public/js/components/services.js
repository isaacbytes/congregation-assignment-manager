builderApp.factory('AuthenticationService', ['$http', '$window', '$injector', '$q', '$timeout', '$location', function($http, $window, $injector, $q, $timeout, $location) {
  var TOKEN_NAME = 'authToken';
  var ATR_SECONDS_FROM_EXP = 10; // Automatic Token Renewal
  
  // holds $timeout promise for deferred token renewal
  var tokenRenewalPromise;
  
  // saves a JWT into localStorage
  var saveToken = function(token) {
    $window.localStorage[TOKEN_NAME] = token;
  };
  
  
  // retrieves JWT from localStorage
  var getToken = function() {
    if (typeof $window.localStorage[TOKEN_NAME] === "string") {
      return $window.localStorage[TOKEN_NAME];
    }
    else {
      return null;
    }
  };
  
  // receives JWT, outputs JSON payload
  var extractPayload = function(token) {
    var payload = JSON.parse($window.atob(token.split('.')[1]));
    return payload;
  };
  
  var registerNewUser = function(user) {
    // check for required params
    if (!user.email || !user.password) {
      console.log('Registration error. Missing credentials.')
      return false;
    }
    
    // proceed
    return $http.post('/api/auth', user).then(function succesCb(res) {
      saveToken(res.authToken);
    }, function errorCb(err) {
      console.log('Error with registration');
      console.log(err);
    });
  }; 
  
  
  var logInUser = function(user) {
    // token expiration in seconds
    var tokenExpiresIn;
    
    // return promise
    return $q(function(resolve, reject) {
      $http.post('/api/auth', user).then(function(response) {
        var currentTime = (Date.now() / 1000),
            tokenExp,
            tokenParsedPayload,
            renewalDelay;
        
        // check response status code
        // save token and return resolve()
        if (response.status === 200) {
          // save token to localStorage
          saveToken(response.data.authToken);
          
          // parse payload
          tokenParsedPayload = JSON.parse($window.atob(response.data.authToken.split('.')[1]));
          
          // calculate expiration (in seconds)
          tokenExpiresIn = tokenParsedPayload.exp - currentTime;
          
          // console status
          console.log('token expires in ' + tokenExpiresIn + ' seconds');
          
          // token renewal delay (in ms)
          renewalDelay = (tokenExpiresIn - ATR_SECONDS_FROM_EXP) * 1000;
          
          // set renewal timer
          tokenRenewalPromise = $timeout(renewToken, renewalDelay);
          
          // console status
          console.log('renewing token in ' + (renewalDelay / 1000) + ' seconds');
          
          // resolve promise
          resolve(response);
        } else {
          reject(response);
        }
        
      }, function(err) {
        reject(err);
      });
    });
    
    
  };
  
  
  // renew JWT, a copy of the logInUser function, with preset args 
  // the 'user' argument in the fn is in this case an arbitrary JSON
  // message so the HTTP request has a body. If not, $http will not
  // post the request
  var renewToken = logInUser.bind(this, {message: 'Renewing token'});
  
  // closes the current session (deletes JWT and disables timer)
  var logOutUser = function() {
    // console logout function
    console.log('Logging out current session...');
    console.log('deleting localstorage auth token...');
    
    
    // delete token from localStorage
    $window.localStorage.removeItem(TOKEN_NAME);
    
    
    // console disable setInterval
    console.log('disabling setInterval...');
    
    // disable setInterval
    if (tokenRenewalPromise) {
      $timeout.cancel(tokenRenewalPromise);
    }
    
    // console logout success
    console.log('successfully logged out');
    
    // redirect to homepage (may or may not require authentication)
    $location.path('/');
  };
  

  // checks to see if user is logged in
  var isLoggedIn = function() {
    // check local storage for token
    var token = getToken();
    
    if (!token) {
      return false;
    }
    
    // [on Safari iOS10.2 my iPad - tested 12/27/16], localStorage
    // returned undefined which passed the if(!token) predicate, 
    // and threw an error when trying to parse a token that wasn't 
    // there. Hence, the isLoggedIn function never finished executing, 
    // and the page was broken. So, the exception is being manually
    // caught in the block below.
    try {
      var payload = JSON.parse($window.atob(token.split('.')[1]));
    } catch (error) {
      return false;
    }
    
    return payload.exp > (Date.now() / 1000);
  };
  
  
  // returns user details of current session
  var currentUser = function() {
    if (isLoggedIn()) {
      var payload = extractPayload(getToken());
      return {
        email: payload.email,
        role: payload.acctType
      }
    }
    else {
      return null;
    }
  };
  
  
  // returns session time remaining (in seconds)
  var sessionTimeRemaining = function() {
    var currentTime = Date.now(),
        tokenPayload,
        timeRemaining;
    
    // make sure user is logged in, read JWT
    if (isLoggedIn) {
      tokenPayload = extractPayload(getToken());
      timeRemaining = tokenPayload.exp - (currentTime / 1000);
      
      console.log('reading session... Seconds remaining: ' + timeRemaining);
      
      // if more than 1 second remaining, return time
      if (timeRemaining > 1) {
        return timeRemaining;
      }
      
      // otherwise return 0 (session expired)
      return 0;
    }
    
    // user is not logged in, return null
    return null;
  }
  
  
  // get general session info
  var getSessionInfo = function() {
    return {
      isLogged: isLoggedIn(),
      secondsRemaining: sessionTimeRemaining(),
      user: currentUser()
    };
  };
  
  // API for managing session via automatic token renewal
  var resumeATR = function() {
    // check if user is logged in
    if (isLoggedIn) {
      // check if timer is set (i.e. no need for resuming)
      if (tokenRenewalPromise) {
        return null;
      }
      
      // check time remaining in current session
      var secondsRemaining = sessionTimeRemaining();
      
      // if fewer seconds than safety measure, log out user
      // added 1 second leeway 
      if (secondsRemaining < (ATR_SECONDS_FROM_EXP + 1)) {
        logOutUser();
        return;
      }
      
      // ** set the timer with the safety measure **
      tokenRenewalPromise = $timeout(renewToken, (secondsRemaining - ATR_SECONDS_FROM_EXP)*1000);
      console.log('successful resume of ATR. Timer set for ' + (secondsRemaining-ATR_SECONDS_FROM_EXP) + ' seconds');
    }
    
    // user is not logged in
    return null;
  };
  
  // expose methods
  return {
    // takes an auth token and saves it to localStorage
    saveToken : saveToken,
    // get raw JWT from localStorage
    getToken : getToken,
    // register a user (function not ready yet)
    registerUser : registerNewUser,
    // takes a user object and submits credentials to API
    logInUser: logInUser,
    // close the current session
    logOutUser: logOutUser,
    // getter (bool), checks if user is logged in
    isLoggedIn: isLoggedIn,
    // gets the logged in user details
    currentUser: currentUser,
    // gets general session information (user, time remaining, etc)
    getSessionInfo: getSessionInfo,
    // renew the current token
    renewToken: function() {
      if (isLoggedIn) {
        renewToken();
      }
    },
    // resume/initiate $timeout for automatic token renewal for current session
    resumeATR: resumeATR
  }
  
}]);



builderApp.factory('GoogleGeoCodeService', ['$resource', function($resource) {
  
  var API_KEY = 'AIzaSyCC7n9z2IHB4Jm_fYA_Cn7TKJqyooFKIX4';
  
  var GoogleService = $resource('https://maps.googleapis.com/maps/api/geocode/json?latlng=40.7330169,-73.893394&location_type=ROOFTOP&result_type=street_address&key=AIzaSyCC7n9z2IHB4Jm_fYA_Cn7TKJqyooFKIX4');
  
  return GoogleService;
  
}]);


builderApp.factory('brotherService', ['$resource', '$http', function($resource, $http) {
  
  var Brother = $resource('/api/brothers/:userid', {userid : '@userid'}, {
    
    update : {
      method : 'PUT'
    }
    
  });
  
  return Brother;
  
}]);


builderApp.factory('mdwkMtgService', ['$resource', '$http', function($resource, $http) {
  
  var MdwkMtg = $resource('/api/mdwkmtgs/:id', {id : '@id'}, {
    
    update : { 
      method : 'PUT'
    }
    
  });
  
  return MdwkMtg;
}]);



builderApp.factory('RotationalColService', ['$resource', function($resource) {
  
  var RotationalCol = $resource('/api/rotationalCols/:colid', {colid : '@colid'}, {
    
    update : {
      method : 'PUT'
    }
    
  });
  
  return RotationalCol;
}]);



builderApp.factory('mdwkMtgServicePerDay', ['$resource', function($resource) {
  // only GET requests for now
  // date in yyyy-mm-dd
  var MdwkMtg = $resource('/api/mdwkmtgs/d/:date', {date : '@date'}, {
    update : {
      method : 'PUT'
    }
  });
  
  return MdwkMtg;
}]);


builderApp.factory('MdwkMtgMonthService', ['$resource', function($resource) {
  // date in mm-yyyy
  var monthData = $resource('/api/mdwkMtgSchedules/:date', {date : '@date'}, {
    save  : {
      method : 'POST',
      isArray : false
    }
  });
  
  return monthData;
}]);


builderApp.factory('ServiceGroupsService', ['$resource', function($resource) {
  
  var serviceGroup = $resource('/api/serviceGroups/:id', {id : '@id'}, {
    save : {
      method: 'PUT',
    }
  });
  
  return serviceGroup;
}]);


builderApp.factory('MdwkMonthIsCompleteService', ['$resource', function($resource) {
  
  // ONLY GET!!
  
  var isComplete = $resource('/api/mdwkMtgSchedules/:date', {date: '@date', q: 'isComplete'});
  
  return isComplete;
  
}]);



builderApp.factory('Sort', ['$filter', function($filter) {
  
  // Provide different sorting methods
  
  return {
    // sort all brothers alphabetically and by role
    roleAlphab : function(broObjs) {
      
      return _.sortBy(_.sortBy(broObjs, function(brother) {
        // first sort by Name
        return $filter('removeAccents')(brother.lastName);
        
        }), function(brother) {
        // then sort by role
        return brother.role;

      });
    }    
  }

}]);


builderApp.factory('DatesUtil', function() {
  
  var todayDate = new Date();
  var todayMoment = moment(todayDate);
  
  
  return {
    
    // get all Mondays of a month, given month and year
    getMondays : function(mmyyyy) {
     
      var momentDate = moment(mmyyyy, 'MM-YYYY'),
          momentMonth = momentDate.month(),
          mondaysArr = [];
      
      
      // if first day of month is NOT monday...
      if (momentDate.day() !== 1) {  
        // get next Monday (if Sunday, set day to one day later)
        momentDate.day() === 0 ? momentDate.day(1) : momentDate.day(8);
      }
      
      // populate array
      while (momentDate.month() === momentMonth) {
        // clone current monday and push
        mondaysArr.push(momentDate.clone());
        
        // increase date to next Monday
        momentDate.day(8);
      }
      
      return mondaysArr;
      
    },
    
    getGlobalTodayMoment : function() {
      
      return todayMoment;
      
    },
    
    cloneTodayMoment : function() {
      return todayMoment.clone();
    }
    
  }
  
});


builderApp.factory('MdwkScaffolding', function() {
  var mdwkMtgScaffold = {
    meta      : {
      date          : null,
      specialWk     : null,
      specialWkMsg  : null,
      chairman      : null, // w/ID
      weeklyBibReading: null,
      introSong     : null,
      midSong       : null,
      concSong      : null,
      concPrayer    : null, // w/ID
      concNotaBene  : null
    },
    treasuresCollection : {
      treasuresCollection: [{
        asgmtCode     : 'TK',
        asgmtTitle    : null,
        duration      : 10,
        hasMedia      : false,
        hasAudParticipation: false
      }, {
        asgmtCode     : 'DG',
        asgmtTitle    : 'Busquemos perlas escondidas',
        duration      : 8,
        hasMedia      : false,
        hasAudParticipation: true
      }, {
        asgmtCode     : 'BR',
        asgmtTitle    : 'Lectura de la biblia',
        duration      : 4,
        hasMedia      : false,
        hasAudParticipation: false
      }]
    },
    applyYourselfCollectionFirstWk : {
      applyYourselfCollection: [{
        asgmtTitle    : 'Preparemos las presentaciones del mes',
        assignee      : [],
        assigneeID    : [],
        asgmtCode     : 'PTP',
        duration      : 15,
        hasMedia      : true,
        hasAudParticipation: true
      }]
    },
    applyYourselfCollectionRegular : {
      applyYourselfCollection: [{
        asgmtTitle    : 'Primera conversación',
        asgmtCode     : 'IC',
        assignee      : [],
        assigneeID    : [],
        duration      : 2,
        hasMedia      : false,
        hasAudParticipation: false
      }, {
        asgmtTitle    : 'Revisita',
        asgmtCode     : 'RV',
        assignee      : [],
        assigneeID    : [],
        duration      : 4,
        hasMedia      : false,
        hasAudParticipation: false
      }, {
        asgmtTitle    : 'Curso bíblico',
        asgmtCode     : 'BS',
        assignee      : [],
        assigneeID    : [],
        duration      : 6,
        hasMedia      : false,
        hasAudParticipation: false
      }]
    },
    livingAsCollection : {
      livingAsCollection: [{
        asgmtCode     : 'TK',
        asgmtTitle    : null,
        duration      : 10,
        assignee      : null,
        hasMedia      : false,
        hasAudParticipation: false
        },{
        asgmtCode     : 'CBS',
        asgmtTitle    : 'Estudio bíblico de congregación',
        duration      : 30,
        assignee      : null,
        reader        : null,
        hasMedia      : false,
        hasAudParticipation: false
        }
      ]
    }
  };
      
  
  
  return {
    
    toggleCoVisit : function(weekObj) {
      var livAsCol = weekObj.livingAsCollection,
          lastPart = livAsCol[livAsCol.length-1];
      
      
      if (lastPart.asgmtCode === 'CBS') {
        lastPart.asgmtCode = 'TK';
        lastPart.asgmtTitle = null;
        delete lastPart.reader;
      }
      
      else {
        lastPart.asgmtCode = 'CBS';
        lastPart.asgmtTitle = 'Estudio bíblico de congregación';
        lastPart.reader = null;
      }
       
    },
    
    
    regularMtgWk : function() {
      var mtgScaffold = {};
      
      return angular.copy(_.extend(
        mtgScaffold,
        mdwkMtgScaffold.meta,
        mdwkMtgScaffold.treasuresCollection,
        mdwkMtgScaffold.applyYourselfCollectionRegular,
        mdwkMtgScaffold.livingAsCollection
      ));
    },
    
    firstWkMtg : function() {
      var mtgScaffold = {};
      
      return angular.copy(_.extend(
        mtgScaffold,
        mdwkMtgScaffold.meta,
        mdwkMtgScaffold.treasuresCollection,
        mdwkMtgScaffold.applyYourselfCollectionFirstWk,
        mdwkMtgScaffold.livingAsCollection
      ));
    },
    
    mtgScaffoldByDate : function(momentDt, isAssemblyWk) {
      var isFirstWk, mtgScaffold = {};
      
      // argument must be a Moment
      if (!moment.isMoment(momentDt)) {
        console.error('Not a moment!');
        return new Error('Fn argument must a Moment');
      }
      
      // argument must be a Monday
      if (momentDt.day() !== 1) {
        console.error('Moment must be a Monday');
        return new Error('Argument must be a Monday!');
      }
      
      // check if first week of month
      isFirstWk = momentDt.date() <= 7;
      
      // scaffold structure depending on first week or not
//      if (isFirstWk) {
//        mtgScaffold = _.extend(
//          mtgScaffold, 
//          mdwkMtgScaffold.meta, 
//          mdwkMtgScaffold.treasuresCollection, 
//          mdwkMtgScaffold.applyYourselfCollectionFirstWk,
//          mdwkMtgScaffold.livingAsCollection
//        );
//      }
//      else {
//        mtgScaffold = _.extend(
//          mtgScaffold, 
//          mdwkMtgScaffold.meta, 
//          mdwkMtgScaffold.treasuresCollection, mdwkMtgScaffold.applyYourselfCollectionRegular,
//          mdwkMtgScaffold.livingAsCollection
//        );
//      }
      
      
      mtgScaffold = _.extend(
        mtgScaffold, 
        mdwkMtgScaffold.meta, 
        mdwkMtgScaffold.treasuresCollection, mdwkMtgScaffold.applyYourselfCollectionRegular,
        mdwkMtgScaffold.livingAsCollection
      );
      
      // add the corresponding date to obj (in ISO-8601 format, so angular an)
      mtgScaffold.date = momentDt.format();
      
      return angular.copy(mtgScaffold);
    }
    
  }
  
  
  
  
});

