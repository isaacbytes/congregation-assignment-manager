angular.module('customDisplayFilters', []).filter('firstInitialLast', function() {
  
  return function(input) {
  
    // input should be a string representing a name, delimited by a space
    // e.g. 'Isaac Farciert', 'Isaac Rigoberto Farciert', 'John Brown Lee'
    var nameComponents,
        filteredString = '',
        concatName = '',
        delimeter;
    
    if (input.includes('/')) { delimeter = '/'; }
    if (input.includes('&')) { delimeter = '&'; }

    
    // support for multiple names (detected by '/')
    if (delimeter) {
      var names = input.split(delimeter); // names = ['Mireya Paradiso', 'Haley Espinoza']
      
      // for every name
      for (var i = 0; i < names.length; i++) {
        var curName = names[i]; // 'Mireya Paradiso'
        var nameComponents;
        
        // break into components
        nameComponents = curName.split(' '); // ['Mireya', 'Paradiso']
        
        // for each name component, except last, extract initial
        for (var j = 0; j < nameComponents.length; j++) {
          var nameComponent = nameComponents[j]; // 'Mireya'
          
          // if not last name... slice
          if (j < (nameComponents.length - 1)) {
            // extract initial
            concatName = concatName + nameComponent.slice(0,1) + '. ';
          }
          
          // if last name, add full name
          else {
            // add full last name
            concatName += nameComponent;
          }      
          
        }
        
        // add '&' to join multiple names
        if (i !== (names.length - 1)) {
          concatName += ' & ';
        }
        
      }
            
      filteredString = concatName;
      return filteredString; 
    }
    
    // single name
    else {
      // array of all strings separated by a space
      nameComponents = input.split(' ');

      // convert all to initials except last name
      for (var i = 0; i < (nameComponents.length - 1); i++) {
        filteredString = filteredString + nameComponents[i].slice(0,1) + ". ";
      }

      // add full last name to initials
      filteredString += nameComponents[nameComponents.length-1]

      return filteredString;
    }
    
  };
  
})

  .filter('fullLengthMapEs', function() {
  
    // db api voc spanish mapping
    var fullMappingEs = {
        elder           : 'Anciano',
        ms              : 'Siervo ministerial',
        other           : 'Otro',
        mdwkchairman    : 'Presidente de Vida y Ministerio',
        mdwkcbsconductor: 'Conductor de estudio bíblico',
        mdwkcbsreader   : 'Lector de estudio bíblico',
        mdwkstudent     : 'Estudiante de Vida y Ministerio',
        mdwkprayer      : 'Oración Vida y Ministerio',
        sunchairman     : 'Presidente de reunion pública',
        sunwtconductor  : 'Conductor de Atalaya',
        sunwtreader     : 'Lector de Atalaya',
        sound           : 'Sonido',
        platform        : 'Plataforma',
        mics            : 'Microfonos',
        attendant       : 'Acomodador',
        pubwitness      : 'Aprobado para predicación pública'
    };

    return function(input) {

      // return mapped value
      if (fullMappingEs[input]) {
        return fullMappingEs[input];
      } else {

        // if none return original value
        return input;
      }

    };
  
  })



  .filter('abbrMapEs', function() {
    var abbrMapEs = {
        elder           : 'A',
        ms              : 'SM',
        other           : 'O',
        mdwkchairman    : 'Pres. VMC',
        mdwkcbsconductor: 'Cond. EBC',
        mdwkcbsreader   : 'Lect. EBC',
        mdwkstudent     : 'Est. VMC',
        mdwkprayer      : 'Oración',
        sunchairman     : 'Pres. At',
        sunwtconductor  : 'Cond. At',
        sunwtreader     : 'Lect. At',
        sound           : 'Son.',
        platform        : 'Plat.',
        mics            : 'Mics.',
        attendant       : 'Acom.',
        pubwitness      : 'Pred. Púb'
    };
    
    return function(input) {
      // return mapped value, if it exists
      if (abbrMapEs[input]) {
        return abbrMapEs[input];
      } else {
        return input;
      }
      
    }
  
  })

  .filter('checkQuant', function() {
    
    return function(input) {
      
      if (Array.isArray(input)) {
        var concatString = '';
        
        input.forEach(function(curVal, index, arr) {
          if (curVal!== null){
            index === 0 ? (concatString += curVal) : (concatString += '/' + curVal); 
          }
        });
        
        return concatString;
        
        
      } else {
        return input;
      }
      
  
      
    }
  
  })




  .filter('httpStatusEs', function() {

    var spanishCodes = {
          404   :   "404. No hubo resultados para esta búsqueda."
    };  
  
  
    return function(input) {
      // return mapped value, if it exists
      if (spanishCodes[input]) {
        return spanishCodes[input];
      } else {
        return input;
      }
      
    }
  
})


.filter('removeAccents', function() {
  
  
    return function(input) {
      
      if (typeof input === "string") {
        return input
        
        .toLowerCase()
        .replace(/Á/g, 'A')
        .replace(/á/g, 'a')
      
        .replace(/É/g, 'E')
        .replace(/é/g, 'e')
      
        .replace(/Í/g, 'I')
        .replace(/í/g, 'i')
      
        .replace(/Ó/g, 'O')
        .replace(/ó/g, 'o')
      
        .replace(/Ú/g, 'U')
        .replace(/ú/g, 'u')
        
        .replace(/Ñ/g, 'N')
        .replace(/ñ/g, 'n');
        
      } else {
        
        return input;
      }
      
    }
  
})

.filter('localeDates', function() {
  
  var monthsEs = {
    
    January     : "enero",
    February    : "febrero",
    March       : "marzo",
    April       : "abril",
    May         : "mayo",
    June        : "junio",
    July        : "julio",
    August      : "agosto",
    September   : "septiembre",
    October     : "octubre",
    November    : "noviembre",
    December    : "diciembre"
    
  };
  
  var monthsEsAbbr = {
    
    Jan         : "ene",
    Feb         : "feb",
    Mar         : "mar",
    Apr         : "abr",
    May         : "may",
    Jun         : "jun",
    Jul         : "jul",
    Aug         : "ago",
    Sep         : "sep",
    Oct         : "oct",
    Nov         : "nov",
    Dec         : "dic"
    
  };
  
  
  return function(input, isAbbr) {
    
    if(isAbbr) {
      return monthsEsAbbr[input] ? monthsEsAbbr[input] : input;
      
    } else {
      return monthsEs[input] ? monthsEs[input] : input;
    }
    
  }
  
  
  
})

// return brothers array filtered by their privileges
.filter('brothersByPrvlg', function() {
  
  return function(brothers, roles, prvlg) {
    
    var filtered = [];
    var filterRoles;
    // ['elder', 'ms']
    
    // for each brother
    brothers.forEach(function(brother) {
      
      // if prvlg provided
      if (prvlg) {
        if (brother.prvlgs.indexOf(prvlg) > -1) {

          // if role is passed in
          if (roles) {
            var found = false;
            filterRoles = roles.split(', ');

            // filter roles
            filterRoles.forEach(function(role) {

              if (!found) {
                if (brother.role === role) {
                  filtered.push(brother);
                  found = true;
                }
              }


            });


          } else {

            filtered.push(brother);
          }

        }
      
      // no prvlg provided, just check for roles
      } else {
        if (roles) {
          var found = false;
          filterRoles = roles.split(', ');

          // filter roles
          filterRoles.forEach(function(role) {

            if (!found) {
              if (brother.role === role) {
                filtered.push(brother);
                found = true;
              }
            }

          });

        // no roles either, just push all items into filtered arr
        } else {
          filtered.push(brother);
        }
        
      }
      
    });
    

    return filtered;
    
  }
  
  
});