// custom token authentication middleware

// Globals
var ACCESS_TOKEN_SECRET       = process.env.JWT_SECRET;

// Deps
var jwt = require('jsonwebtoken');
var User = require('../models/user');
var _ = require('underscore');

// Functions
function sendJsonResponse(res, status, content) {  
  res.status(status);
  res.json(content);
}


/* **************** MIDDLEWARE FUNCTION **********/

var authenticate = function(req, res, next) {
  /* Variables */
  var authHeader, rawToken,
      tokenIndex, isActive;
  
  // Grab Authorization Header value
  authHeader = req.get('Authorization') || '';
  
  // Isolate the token by spliting value by space
  // and grabbing last value
  rawToken = authHeader.split(/\s+/).pop() || '';
  
  // If token is not present, exit
  if (!authHeader || !rawToken) {
    sendJsonResponse(res, 401, {message: 'This action requires authentication. Please present credentials'});
    return;
  }
  
  // 1. Verify token signature and expiration
  jwt.verify(rawToken, ACCESS_TOKEN_SECRET, function(err, decodedToken) {
    
    // failed jwt verification
    if (err) {
      sendJsonResponse(res, 401, err);
      return;
    }
    
    // passed jwt verification
    
  // 2. Check User profile for token
    User.findById(decodedToken.user_id, function(err, user) {
      
      // catch Mongoose find() error
      if (err) {
        sendJsonResponse(res, 500, {message: 'Something went wrong with your request, please try again.', error: err});
        return;
      }
      
      // if user not found
      if (!user) {
        sendJsonResponse(res, 401, {message: 'Token invalid. Please sign in to request another token', error: 'User not found'});
        return;
      }
      
      // user found, check user.tokens array for token
      tokenIndex = _.findIndex(user.tokens, { tokenStr: rawToken });
      
      // if token not found in profile, exit
      if (tokenIndex < 0) {
        sendJsonResponse(res, 401, {message: 'Token invalid. Please sign in to request another token', error: 'Token not in user profile'});
        return;
      } 
      
      // check if token has been invalidated (revoked or logged out)
      isActive = user.tokens[tokenIndex].active;
      
      // if invalidated, exit
      if (!isActive) {
        sendJsonResponse(res, 401, {message: 'Token invalid. Please sign in to request another token', error: 'Token has been invalidated'});
        return;
      }
      
      // all went well, attach token to request object
      req.authTokenRaw = rawToken;
      req.authTokenDecoded = decodedToken;
      req.authTokenUser = user; 
        
  // 3. Proceed to next middleware function
      next();
    });
    
    
  });
  
};


module.exports = authenticate;