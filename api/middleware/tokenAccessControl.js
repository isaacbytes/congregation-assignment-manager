// custom access control middleware

// --------------------------------------------------------- //
// * assumes token has already been parsed and validated,
// * and attached to request object
// ** req.authTokenRaw - base64 encoded
// ** req.authTokenDecoded - JS object
// ** req.authTokenUser - user profile from Users collection
// --------------------------------------------------------- //


// Deps

// Functions
function sendJsonResponse(res, status, content) {  
  res.status(status);
  res.json(content);
}


/* **************** MIDDLEWARE FUNCTIONS **********/
var onlyAllowAtLeast = {
  
  // only admins can perform this action
  admin : function(req, res, next) {
    // check DB record for account type
    var acctType = req.authTokenUser.acctType;
    
    // if not admin, exit with a 401
    if (acctType !== 'admin') {
      sendJsonResponse(res, 401, { message: 'Sorry. You are not authorized to perform this action.', minDesignation: 'admin' });
      return;
    }
    
    // access granted, invoke next middleware
    next();
  },
  
  // only publishers (and higher) can perform this action
  publishers : function(req, res, next) {
    // check DB record for account type
    var acctType = req.authTokenUser.acctType;
    console.log('acctType: ' + acctType);
    
    // if not AT LEAST publisher, exit with a 401
    if (acctType !== 'pub' && acctType !== 'admin' && acctType !== 'elder') {
      sendJsonResponse(res, 401, { message: 'Sorry. You are not authorized to perform this action.', minDesignation: 'pub' });
      return;
    }
    
    // access granted, invoke next middleware
    next();
  },
  
  // only elders (and higher) can perform this action
  elders : function(req, res, next) {
    // more safekeeping
    var acctType = req.authTokenUser.acctType;
    
    if (acctType !== 'elder' && acctType !== 'admin') {
      sendJsonResponse(res, 401, { message: 'Sorry. You are not authorized to perform this action', minDesignation: 'elder' });
      return;
    }
  
    next();
  }
  
};


module.exports = onlyAllowAtLeast;