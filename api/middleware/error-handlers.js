// custom error handlers for inserting as Express middleware

// abstracts appropriate res.status() and res.json() into one call
function sendJsonResponse(res, status, content) {  
  res.status(status);
  res.json(content);
}


module.exports = {
  
  sendJsonPassportErrors: function(err, req, res, next) {
    var statusCode = err.status || 401;
    
    sendJsonResponse(res, statusCode, err);
    
  }
  
}