// routes/users.js

// ** all actions should be protected
// ** mostly admin access only

// deps
var User = require('../models/user');
var usersCtrl = require('../controllers/users');
var tokenDoubleAuth = require('../middleware/tokenDoubleAuthentication');
var onlyAllow = require('../middleware/tokenAccessControl');


module.exports = function(app) {
  
  
// [GET] all users
app.get('/api/users', usersCtrl.getAllUsers);
  
// [GET] a single user by id
app.get('/api/users/:id', usersCtrl.getOneUser)
  
// [POST] handle new user registration
app.post('/api/users', usersCtrl.createUser);

// [PUT] user editing
app.put('/api/users', usersCtrl.editUser);

// [DELETE] user deletion
app.delete('/api/users/:id', usersCtrl.deleteUser);

  
}