// api/route/serviceGroup.js


var ServiceGroup = require('../models/serviceGroup');
var serviceGroupCtrl = require('../controllers/serviceGroup');
var tokenDoubleAuth = require('../middleware/tokenDoubleAuthentication');
var onlyAllow = require('../middleware/tokenAccessControl');


module.exports = function(app) {
  
  
  // GET all service groups
  app.get('/api/serviceGroups', tokenDoubleAuth, serviceGroupCtrl.serviceGroupsGetAll);
  
  // GET one service group by id (num) 
  app.get('/api/serviceGroups/:id', tokenDoubleAuth, serviceGroupCtrl.serviceGroupsGetOneById);
  
  // POST - create a new service group
  app.post('/api/serviceGroups', tokenDoubleAuth, serviceGroupCtrl.serviceGroupsCreateOne);
  
  // PUT - update a service group by id
  app.put('/api/serviceGroups/:id', tokenDoubleAuth, serviceGroupCtrl.serviceGroupsUpdateOneById);
  
  // DELETE a service group by id
  app.delete('/api/serviceGroups/:id', tokenDoubleAuth, serviceGroupCtrl.serviceGroupsDeleteOneById);
  
};