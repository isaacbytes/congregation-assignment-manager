// api/routes/rotationalData.js

// Rotational data controller
var rotationalDataCtrl = require('../controllers/rotationalData');
var tokenDoubleAuth = require('../middleware/tokenDoubleAuthentication');
var onlyAllow = require('../middleware/tokenAccessControl');

module.exports = function(app) {
  
  // GET rotational data for a given week ('MM-DD-YYYY')
  app.get('/api/rotationalData/d/:date', tokenDoubleAuth, rotationalDataCtrl.rotationalDataGetWk);
  
  app.get('/api/rotationalData/:date', tokenDoubleAuth, rotationalDataCtrl.rotationalDataGetMonths);
  
  // date range in format 'MM*MM-YYYY' OR 'MM-YYYY*MM-YYYY' 
  // date range in format ?f=MM-YYYY&t=MM-YYYY
  app.post('/api/rotationalData/', tokenDoubleAuth, rotationalDataCtrl.rotationalDataCreateMonths);
  
};