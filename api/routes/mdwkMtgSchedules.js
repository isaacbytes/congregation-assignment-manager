// app/routes/mdwkMtgSchedules.js

var MdwkMtg = require('../models/mdwkMtg');
var MdwkMonthCtrl = require('../controllers/mdwkMonthSchedules');
var moment = require('moment');
var tokenDoubleAuth = require('../middleware/tokenDoubleAuthentication');
var onlyAllow = require('../middleware/tokenAccessControl');

// add the routes

module.exports = function(app) {
  
  var today = moment(new Date());
  
  
  // GET all mdwkMtg Schedules in a specified month and year
  app.get('/api/mdwkMtgSchedules/:date', tokenDoubleAuth, MdwkMonthCtrl.getMonthData);
  
  // CREATE/UPDATE a midweek meeting
  app.post('/api/mdwkMtgSchedules/:date', MdwkMonthCtrl.createOrUpdateMonth);  

  
// end module.exports
};