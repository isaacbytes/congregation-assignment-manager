// app/routes/auth.js 


var User = require('../models/user');
var authCtrls = require('../controllers/auth');
var tokenDoubleAuth = require('../middleware/tokenDoubleAuthentication');

 
// add the routes
module.exports = function(app) {
    
  // POST - receive username/password, receive access token for resource API
  // OR receive valid auth token, receive renewed token for resource API
  app.post('/api/auth', authCtrls.checkCredentials);
  
  app.post('/api/authLogOut', tokenDoubleAuth, authCtrls.logOutToken);
  
};