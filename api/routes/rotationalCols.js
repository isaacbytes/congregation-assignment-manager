// api/routes/rotationalCols.js

var RotationalCol = require('../models/rotationalCol');
var RotationalColCtrl = require('../controllers/rotationalCols');
var tokenDoubleAuth = require('../middleware/tokenDoubleAuthentication');
var onlyAllow = require('../middleware/tokenAccessControl');

module.exports = function(app) {
  
  
  // get all prvlg columns
  app.get('/api/rotationalCols/', tokenDoubleAuth, RotationalColCtrl.rotationalColsGetAll);
  
  // get a single prvlg column by column id
  app.get('/api/rotationalCols/:colid', tokenDoubleAuth, RotationalColCtrl.rotationalColsGetOneById);
  
  // create a prvlg column
  app.post('/api/rotationalCols/:colid', tokenDoubleAuth, RotationalColCtrl.rotationalColsCreateOneById);
  
  // update a prvlg column
  app.put('/api/rotationalCols/:colid', tokenDoubleAuth, RotationalColCtrl.rotationalColsUpdateOneById);
  
  // delete a prvlg column
  app.delete('/api/rotationalCols/:colid', tokenDoubleAuth, RotationalColCtrl.rotationalColsDeleteOneById);
  
  
};