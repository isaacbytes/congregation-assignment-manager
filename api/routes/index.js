// app/routes/index.js
// ** index for all routes


module.exports = function(app) {
  

  // API routes
  require('./brothers')(app);
  require('./mdwkMtgs')(app); 
  require('./mdwkMtgSchedules')(app); 
  require('./rotationalCols')(app);
  require('./rotationalData')(app);
  require('./serviceGroup')(app);
  require('./users')(app);
  require('./auth')(app);
  
  
};