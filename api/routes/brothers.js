// app/route/brothers.js 


var Brother = require('../models/brother');
var brotherCtrls = require('../controllers/brothers');
var tokenDoubleAuth = require('../middleware/tokenDoubleAuthentication');
var onlyAllow = require('../middleware/tokenAccessControl');

// discard
var passport = require('passport');
var errorHandlers = require('../middleware/error-handlers');
var passportConfig = {
  session: false,
  failWithError: true // throws error for us to catch to send JSON
};


// add the routes
module.exports = function(app) {
    
  // GET ALL brothers or BY PRIVELEGE
  app.get('/api/brothers',
          tokenDoubleAuth, // token auth
//          onlyAllow.elders, // access control
          brotherCtrls.brothersGetAll, // main controller
          // maybe add filter middleware ? 
          errorHandlers.sendJsonPassportErrors // error catching
  );
  
  
  // GET a single brother
  app.get('/api/brothers/:id', tokenDoubleAuth, brotherCtrls.brothersGetOneById);
  
  
  // CREATE a new brother
  app.post('/api/brothers', tokenDoubleAuth, brotherCtrls.brothersCreateOne);
  
  
  // PUT - update a single brother
  app.put('/api/brothers/:id', tokenDoubleAuth, brotherCtrls.brothersUpdateOneById);
  
  
  // DELETE a brother by id
  app.delete('/api/brothers/:id', tokenDoubleAuth, brotherCtrls.brothersDeleteOneById);

   

};