// app/route/mdwkMtgs.js 
var MdwkMtg = require('../models/mdwkMtg');
var moment = require('moment');
var mdwkMtgCtrl = require('./../controllers/mdwkmtgs');
var dateHelpers = require('./../libs/dates');
var tokenDoubleAuth = require('../middleware/tokenDoubleAuthentication');
var onlyAllow = require('../middleware/tokenAccessControl');

// moment.js expected date format
var inputDateFormat = "MM-DD-YYYY";

 
// add the routes
module.exports = function(app) {
    
  // GET ALL midweek Meetings
  app.get('/api/mdwkMtgs', tokenDoubleAuth, mdwkMtgCtrl.mdwkMtgsGetAll);
  
  
  // GET a single Meeting by ID
  app.get('/api/mdwkMtgs/:id', tokenDoubleAuth, mdwkMtgCtrl.mdwkMtgsGetOneById);
  
  
  // GET a single Meeting by DATE
  app.get('/api/mdwkMtgs/d/:date', tokenDoubleAuth, mdwkMtgCtrl.mdwkMtgsGetOneByDate);
  
  
  // POST a single Meeting by DATE
  app.post('/api/mdwkMtgs/d/:date', tokenDoubleAuth, mdwkMtgCtrl.mdwkMtgsCreateOneByDate);
  
  
  // DELETE a meeting by id
  app.delete('/api/mdwkMtgs/:id', tokenDoubleAuth, mdwkMtgCtrl.mdwkMtgsDeleteOneById);
  
  
  // PUT - update a single meeting
  app.put('/api/mdwkMtgs/:id', tokenDoubleAuth, mdwkMtgCtrl.mdwkMtgsUpdateOneById);
  
  app.get('/api/mondaysResource/:date', tokenDoubleAuth, mdwkMtgCtrl.dateResource);
  
  
};