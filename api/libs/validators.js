// validators for Mongoose schemas 
var moment = require('moment');
var _ = require('underscore');
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var ACCESS_TOKEN_SECRET = process.env.JWT_SECRET;

module.exports = {
  // API-LEVEL VALIDATION
  bearerStrategyConfigFn : function(token, done) {
    
    // Step 1. Validate Token
    jwt.verify(token, ACCESS_TOKEN_SECRET, function(err, decodedToken) {
      // failed jwt.verify()
      if (err) {
        return done(null, false, { message: err.message });
      }
      
      
      // passed jwt.verify()
      // Step 2. Extract username, proceed with double-verification
      User.findById(decodedToken.user_id, function(err, user) {
        var tokenIndex,
            isActive;
        
        // if user not found, or other error, return false
        if (!user) {
          return done(null, false, { message: 'User does not exist.'});
        }
        
        // if err, return system error 
        if (err) {
          return done(err);
        }
        
        // user found, proceed with double-verification
        // search for token in user profile
        tokenIndex = _.findIndex(user.tokens, { tokenStr: token });
        
        // if token, not found, do not authenticate
        if (tokenIndex < 0) {
          return done(null, false, { message: 'Token has expired. Please sign in to request another token. Token not in user profile.'});
        }
        
        isActive = decodedToken.active;
        
        // if token has been blacklisted, or revoked, (or logged out), do not authenticate
        if (!isActive) {
          return done(null, false, { message: 'Token has expired. Please sign in to request another token. Token marked as invalidated.'})
        }
        
        // all went well, return user profile back to controller
        return done(null, user); 
      });
    });  
    
  },
  
  // User Registration, password validation
  validatePassword : function(password) {
    
  },
  
  // Token Authentication
  validateTokenDoubleVerification : function(token) {
 
    
  },
  
  // Brother validators
  valBrothers : {
    validatePrvlgs : function() {
      
    }
  },
  
  // Midweek meeting validators
  valMdwkMtgs : {
    
    valMonthByDate : function(momentDt, monthArr) {
      /* **** VALIDATE PARAMS **** */
      
      if (!momentDt || !monthArr) {
        throw new Error('Fn missing parameters');
      }
      else if (!moment.isMoment(momentDt)) {
        throw new Error('First argument must be an instance of Moment');
      }
      
      /* ************************* */
      
      var momentClone;
      
      // Do not mutate Moment, clone it
      momentClone = moment(momentDt);
      
      /* ********************
            ***** UNFINISHED!!! *****
        *********************
      */
      
    }
   
    
  },
  
  
  
  // Misc.
  valDate : function(dateString, format) {
    // check for missing param
    if (!dateString) {
      throw new Error('Missing date string parameter!');
    }
    
    // default date format for validity
    var format = format || 'YYYY-MM-DD',
        formats = format.split(' '),
        isoFormat = formats.indexOf('moment.ISO_8601');
    
    // replace string literal with constant in case of ISO date 
    if (isoFormat > -1) {
      formats[isoFormat] = moment.ISO_8601;
    }

    return moment(dateString, formats, true).isValid();
  },
  
  
  
  // returns object for querying a specific date in Mongoose
  getMongooseQDateObj : function(momentDt, period) {
    if (!moment.isMoment(momentDt)) {
      throw new Error('Date argument must be an instance of Moment');
    }
    
    var period = period || 'day'; // 'day' || 'month' || 'year'
    var startDelimit,
        endDelimit;
    
    if (period === 'day') {
      startDelimit = moment(momentDt).startOf('day'),
      endDelimit = moment(momentDt).add(1, 'days');
    }
    
    else if (period === 'month') {
      startDelimit = moment(momentDt).startOf('month');
      endDelimit = moment(momentDt).endOf('month');
    }
    
    else if (period === 'year') {
      startDelimit = moment(momentDt).startOf('year');
      endDelimit = moment(momentDt).endOf('year');
    }
    
    return {
      'date' : {
        '$gte' : startDelimit.toDate(),
        '$lt' : endDelimit.toDate()
      }
    };
  },
  
  
  // transforms Moment arg into Monday of week
  toMonday : function(momentDt) {
    var momClone;
    if (!moment.isMoment(momentDt)) {
      throw new Error('Date argument must be an instance of Moment');
    }
    
    // do not mutate argument, clone it
    momClone = moment(momentDt);
    
    // If not a monday, transform to monday
    if (momClone.day() !== 1) {
      // If day is sunday (0)
      if (momClone.day() === 0) {
        momClone.subtract(6, 'days');
      }
      // every other day
      else {
        momClone.subtract((momClone.day()-1), 'days');
      } 
    }
    return momClone;
  },
  
  
  // get all Mondays in a month ('MM-YYYY')
  getMondaysByMonth : function(monthYearString) {
    var momentDate,
        momentMonth,
        mondaysArr = [];
    
    // if moment passed as arg, save it
    if (moment.isMoment(monthYearString)) {
      momentDate = monthYearString.clone().startOf('month');
      momentMonth = momentDate.month();
    }
    
    // else, make sure string is in 'MM-YYYY' format
    else if (!moment(monthYearString, 'MM-YYYY', true).isValid()) {
      throw new Error('Month/Year must be in "MM-YYYY" format');
    }
    
    // format is good, create moment
    else {
      momentDate = moment(monthYearString, 'MM-YYYY', true).startOf('month');
      momentMonth = momentDate.month();
    }
    
    
    
    // if first day of month is NOT monday...
    if (momentDate.day() !== 1) {  
      // get next Monday (if Sunday, set day to one day later)
      momentDate.day() === 0 ? momentDate.day(1) : momentDate.day(8);
    }

    // populate array
    while (momentDate.month() === momentMonth) {
      // clone current monday and push
      mondaysArr.push(momentDate.clone());

      // increase date to next Monday
      momentDate.day(8);
    }

    return mondaysArr;
  }
  
    
  
};