// helper date functions
module.exports = {
  
  
  // takes a valid Moment and transforms it into the Monday of that week
  toMonday : function(momentDate) {
    var diff;
    
    // only if date is not already a Monday
    if (momentDate.day() !== 1) {
      
      // if it's a Sunday, make sure to get correct week (6 days before)
      if (momentDate.day() === 0) {
        momentDate.subtract(6, 'days');
      }
      
      // for every other day
      else {
        diff = momentDate.day() - 1;
        momentDate.subtract(diff, 'days');
      }
    }
  }
  
  
}; 