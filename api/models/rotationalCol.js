// ** API MODEL ** //
// api/models/rotationalCol.js

var mongoose = require('mongoose');
var _ = require('underscore');


var schema = {
  
  
  _id: {
    type              : String,   // 'mdwkchairman'
    required          : true
  },
  
  priority: {
    type              : Number,   // 17
    required          : true
  },
  
  templatePrvlg: {
    type              : String,   // 'mdwkchairman'
    required          : true
  },
  
  mtgLoc: {
    type              : String,   // 'mdwk'
    required          : true
  },
  
  locOrder: {
    type              : Number,   // 1
    required          : true
  },
  
  inactive: {
    type              : Boolean,
    required          : true
  }
  
};



var RotationalColSchema = new mongoose.Schema(schema);

module.exports = mongoose.model('RotationalCol', RotationalColSchema);