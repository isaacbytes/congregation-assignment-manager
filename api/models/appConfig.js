// App-level config file (initially populated on reset/new account/initial bootstrap)
var mongoose = require('mongoose');


/* ******************** SCHEMA ******************** */
var notificationsSubSchema = {
  kind : {
    type          : String,
    enum          : ['']
  },
  msg             : String
};


var schema = {
  
  isNew                             : Boolean,
  congregationName                  : String,
  
  adminSettings: {
    firstName                       : String,
    lastName                        : String,
    email                           : String,
    hash                            : String,
    notifications                   : [notificationsSubSchema]
  },
  
  systemNotifications               : [notificationsSubSchema]
  
};


var appConfigSchema = new mongoose.Schema(schema);



/* ******************** Exports ******************** */
module.exports = mongoose.model('AppConfig', appConfigSchema);