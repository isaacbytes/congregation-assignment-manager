// ** API MODEL ** //
// api/models/rotationalData.js

var mongoose = require('mongoose');
var _ = require('underscore');


var schema = {
  
  // meta 
  date: {
    type              : Date,
    required          : true
  },
  
  
  canceledMtgs: {
    type: [{
      type            : String,
      enum            : ['mdwk', 'wknd']
    }],
    required          : false
  },
  
  prvlgs: {
    type: [{
      
      prvlg: {
        type          : String,
        ref           : 'RotationalCol',
        required      : true
      },
      
      assignee: {
        type          : mongoose.Schema.ObjectId,
        ref           : 'Brother',
        required      : true
      }
      
    }],
    
    required          : true
  }
  
};



var RotationalDataSchema = new mongoose.Schema(schema);

module.exports = mongoose.model('RotationalData', RotationalDataSchema);