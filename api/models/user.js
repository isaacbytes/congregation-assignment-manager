// app/models/user.js


/* ************** DEPS **************** */
var mongoose        = require('mongoose');
var moment          = require('moment');
var bcrypt          = require('bcrypt');
const SALT_ROUNDS   = 10;




/* ************** README ****************

  -- note that the user password is only hashed  --
  -- when the 'save()' method is used to create  --
  -- and update passwords. If not, they'll be in --
  -- PLAINTEXT. ALWAYS perform save() operations --

*/



/* ************** SCHEMA **************** */
var schema = {
  
  firstName : {
    type            : String,
    required        : true
  },
  
  lastName : {
    type            : String,
    required        : true
  },
  
  email : {
    type            : String,
    required        : true
  },
  
  password : {
    type            : String,
    required        : true
  },
  
  acctType : {
    type            : String,
    enum            : ['admin', 'pub', 'elder'],
    required        : true
  },
  
  accessPrvlgs : {
    type            : [String],
    enum            : ['root', 'mdwkMtg-view', 'mdwkMtg-edit', 'mdwkMtg-create',
                               'brothers-view', 'brothers-edit', 'brothers-create']
  },
  
  linkedBrotherAcct : {
    type            : mongoose.Schema.ObjectId,
    ref             : 'Brother',
    required        : false
  },
  
  tokens: {
    type: [{
      platform: {
        type        : String,
        enum        : ['browser', 'iOS'],
        required    : true
      },
      
      active: {
        type        : Boolean,
        required    : true
      },
      
      tokenStr: {
        type        : String,
        required    : true
      }
    }]
  }
  
};

var userSchema = new mongoose.Schema(schema);



/* ************** SCHEMA METHODS **************** */

// hashes password and saves to 'hash' field in db
userSchema.methods.setPassword = function(password) {
  var hash = bcrypt.hashSync(password, SALT_ROUNDS);
  this.password = hash;
};

// compares proposed password to password in DB
userSchema.methods.validatePassword = function(password) {
  var valid = bcrypt.compareSync(password, this.password);
  return valid;
};

// generates JWT
userSchema.methods.generateJwt = function() {
};


/* ************** 'SAVE' HOOKS **************** */
//userSchema.pre('save', function(next) {
//  
//  var user = this;
//  
//  // if password has not been modified, do not generate new hash
//  if (!user.isModified('password')) {
//    return next();
//  }
//  
//  // generate salt
//  bcrypt.genSalt(SALT_ROUNDS, function(err, salt) {
//    
//    if (err) return next(err);
//    
//    // hash password with salt
//    bcrypt.hash(user.password, salt, function(err, hash) {
//      
//      if (err) return next(err);
//      
//      // replace plaintext password with hash
//      user.password = hash;
//      next();
//    
//    });
//    
//  });
//  
//});




/* ************** EXPORTS **************** */
module.exports = mongoose.model('User', userSchema);



