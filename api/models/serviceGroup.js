// app/models/serviceGroup.js

// deps
var mongoose = require('mongoose');
var _ = require('underscore');

var schema = {
  
  _id: {
    type                  : Number,
    required              : true
  },
  
  overseer: {
    type                  : mongoose.Schema.ObjectId,
    ref                   : 'Brother',
    required              : true
  },
  
  aux: {
    type                  : mongoose.Schema.ObjectId,
    ref                   : 'Brother',
    required              : false
  },
  
  assignees: { 
    type: [{ 
      type                : mongoose.Schema.ObjectId,
      required            : true
    }],
    required              : false
  },
  
  satLoc: {
    type                  : String,
    required              : false
  },
  
  sunLoc: {
    type                  : String,
    required              : false
  }
  
};

var serviceGroupSchema = new mongoose.Schema(schema);


module.exports = mongoose.model('ServiceGroup', serviceGroupSchema)