// app/models/mdwkMtg.js
// new git branch!

var moment = require('moment');
var _ = require('underscore');
var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var validatorFn = function(v) {
  if (this.specialWk === 'assembly') {
    return true;  
  } 
  else if (!v) {
    return false;
  }
}

var regularWkRequired = [validatorFn, '{PATH} is required in regular week']; 


// define Schema
var mdwkMtgSchema = new mongoose.Schema({

  // ***** meta and single values ***** //
  date : {
    type                  : Date,
    required              : true
  },
  isStarted : {
    type                  : Boolean,
    "default"             : false
  },
  isComplete : {
    type                  : Boolean,
    "default"             : false
  },
  specialWk : {
    type                  : String,
    enum                  : ['co', 'assembly', 'other'],
    lowercase             : true,
  },
  specialWkMsg            : String,
  themeColor              : String,
  chairman : { 
    type                  : String,
    validate              : regularWkRequired
  },              
  chairmanID              : Schema.ObjectId,
  weeklyBibReading : { 
    type                  : String,
    validate              : regularWkRequired
  },              
  introSong : { // temp - required
    type                  : Number,
    validate              : regularWkRequired
  },              
  midSong  : { // temp - required
    type                  : Number,
    validate              : regularWkRequired
  },              
  concSong  : { // temp - required
    type                  : Number,
    validate              : regularWkRequired
  },
  concNotaBene : {
    type                  : String,
    required              : false
  },
  concPrayer : { 
    type                  : String,
    validate              : regularWkRequired
  },              
  concPrayerID            : Schema.ObjectId,
  
  // ***** Meeting Sections Collections ***** //
  treasuresCollection :   {
    type: [
    {
      asgmtTitle : {
        type              : String,
        required          : true
      },         
      asgmtCode : {
        type              : String,
        enum              : ['TK', 'DG', 'BR'],
        required          : true
      },
      duration : { 
        type              : Number,
        required          : true
      },        
      assignee : {
        type              : String,
        required          : true
      },        
      assigneeID          : Schema.ObjectId,
      assigneeSecondary   : String,
      assigneeSecondaryID : Schema.ObjectId,
      hasMedia            : Boolean,
      hasAudParticipation : Boolean,
      description         : String
    }
  ],
    
  validate                : regularWkRequired

  },
  
  applyYourselfCollection : {
    type: [
      {
        asgmtTitle : {
          type              : String,
          required          : true
        },
        asgmtCode : {
          type              : String,
          enum              : ['IC', 'RV', 'BS', 'PTP'],
          required          : true
        },
        duration : {
          type              : Number,
          required          : true
        },
        assignee : {
          type              : [String],
          required          : true
        },
        assigneeID          : [Schema.ObjectId],
        assigneeSecondary   : [String],
        assigneeSecondary   : [Schema.ObjectId],
        hasMedia            : Boolean,
        hasAudParticipation : Boolean,
        description         : String

      }
    ],
    validate : regularWkRequired
  },
  
  livingAsCollection : {
    type: [
      {
        asgmtTitle : {
          type              : String,
          required          : true
        },
        asgmtCode : {
          type              : String,
          enum              : ['TK', 'CBS'],
          "default"         : 'TK',
          required          : true
        },
        duration : {
          type              : Number,
          required          : true
        },
        assignee : {
          type              : String,
          required          : true
        },
        assigneeID          : Schema.ObjectId,
        assigneeSecondary   : String,
        assigneeSecondaryID : Schema.ObjectId,
        reader              : String,
        readerID            : Schema.ObjectId,
        hasMedia            : Boolean,
        hasAudParticipation : Boolean,
        description         : String

      }
    ],
    validate : regularWkRequired
  } 
  
});


mdwkMtgSchema.pre('findOneAndUpdate', function() {
  const update = this.getUpdate();
  if (update.__v != null) {
    delete update.__v;
  }
  const keys = ['$set', '$setOnInsert'];
  for (const key of keys) {
    if (update[key] != null && update[key].__v != null) {
      delete update[key].__v;
      if (Object.keys(update[key]).length === 0) {
        delete update[key];
      }
    }
  }
  update.$inc = update.$inc || {};
  update.$inc.__v = 1;
});



// export a new Mongoose model with the mdwkMtg Schema
module.exports = mongoose.model('MdwkMtg', mdwkMtgSchema);




// asgmtCode CODE DEFINITIONS
/*  
    TK - Initial Talk
    DG - Digging for Spiritual Gems
    BR - Bible Reading
    --------------------
    IC - initial call
    RV - return visit 
    BS - bible study
    PTP - prepare this month's presentations
    --------------------
    TK - [default] misc. talk
    CBS - congregation bible study
*/
