// app/models/brothers.js

// grab deps
var mongoose          = require('mongoose');
var _                 = require('underscore');

/* ************** SCHEMA **************** */
var schema = {
  
  firstName : {
    type                : String,
    required            : true
  },
  lastName : {
    type                : String,
    required            : true
  },
  gender : {
    type                : String,
    enum                : ['m', 'f'],
    lowercase           : true,
    required            : true
  },
  role : {
    type                : String,
    enum                : ['elder', 'ms', 'other'],
    lowercase           : true,
    required            : true,
  },
  isPublisher : {
    type                : Boolean,
    required            : true,
  },
  isBaptized : {
    type                : Boolean,
    required            : true,
  },
  prvlgs : {
    type : [{
      type              : String,
      enum              : ['mdwkchairman', 'mdwkcbsconductor', 'mdwkcbsreader', 'mdwkstudent', 'mdwkprayer', 'sunchairman', 'sunwtreader', 'sound', 'platform', 'mics', 'attendant', 'pubwitness', 'regpio', 'auxpio'],
      lowercase         : true
    }],
    required            : false
  },
  address: {
    type                : String,
    required            : false
  },
  
  /* READONLY PROPERTIES */
  created : {
    type                : Date,
    "default"           : Date.now()
  },
  serviceGroup : { 
    type                : Number,
    ref                 : 'ServiceGroup',
    required            : false
  }, 
  registeredAccount : {
    type                : mongoose.Schema.Types.ObjectId,
    ref                 : 'User',
    required            : false
  }
  
};


var brotherSchema = new mongoose.Schema(schema);



// define our model in exports so we can pass it to other files when called
module.exports = mongoose.model('Brother', brotherSchema);



/* 

// *************** MIDWEEK MEETING *************** //

** mdwk chairman : mdwkchairman

** congregation bible study conductor : cbsconductor

** congregation bible study reader : cbsreader

*? midweek student : mdwkstudent ?*

** midweek conclusion prayer : mdwkprayer


// *************** MISC *************** //

** sound : sound

** platform : platform

** mics : mics

** attendants : attendant

** public witnessing approved : pubwitness



// *************** SUNDAY MEETING *************** //

** sunday chairman : sunchairman

** watchtower conductor : sunwtconductor

** watchtower reader : sunwtreader




*/