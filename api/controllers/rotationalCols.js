/* ** API CONTROLLER ** */

var mongoose = require('mongoose');
var _ = require('underscore');
var RotationalColModel = require('../models/rotationalCol');

module.exports = {
  
  // get all rotational columns in db
  rotationalColsGetAll: function(req, res) {
    
    RotationalColModel.find(function(err, cols) {
      // Error-trap 1: Check for empty return set
      if (cols.length < 1) {
        sendJsonResponse(res, 404, { message: 'No rotational columns found' });
        return;
      }
      
      // Error-trap 2: Catch any Mongoose/Mongo errors
      else if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      
      sendJsonResponse(res, 200, cols);
    });
  },
  
  
  
  // get a single rotational column by id
  rotationalColsGetOneById: function(req, res) {
    // Error-trap 1: Check for column id 
    if (!req.params.colid) {
      sendJsonResponse(res, 400, { message: 'Missing column id' });
      return;
    }
    
    RotationalColModel.findById(req.params.colid, function(err, col) {
      
      // Error-trap 2: Check for empty return set
      if (!col) {
        sendJsonResponse(res, 404, { message: 'No columns found with that id' });
        return;
      }
      
      // Error-trap 3: Catch any Mongoose/Mongo errors
      if (error) {
        sendJsonResponse(res, 404, err);
        return;
      }
      
      
      sendJsonResponse(res, 200, col);
    });
  },
  
  
  
  rotationalColsCreateOneById: function(req, res) {
    // Error-trap 1: Check for column id
    if (!req.params.colid) {
      sendJsonResponse(res, 400, { message: 'Missing column id' });
      return;
    }
    
    // transform request object to include column id as '_id' field
    req.body._id = req.params.colid;
    
    
    RotationalColModel.create(req.body, function(err, col) {
      // Error-trap 2: Catch Mongoose/Mongo errors
      if (err) {
        sendJsonResponse(res, 400, err);
        return;
      }
      
      sendJsonResponse(res, 200, col);
    });
  },
  
  
  
  rotationalColsUpdateOneById: function(req, res) {
    console.log('Tri');
    // Error-trap 1: Check for column id  
    if (!req.params.colid) {
      sendJsonResponse(res, 400, { message: 'Missing column id' });
      return;
    }
    
    RotationalColModel.findById(req.params.colid, function(err, col) {
      // Error-trap 2: Catch Find Errors from Mongoose/Mongo
      if (err) {
        sendJsonResponse(res, 400, err);
        return;
      }
      
      // Error-trap 3: Check for empty response
      if (!col) {
        sendJsonResponse(res, 404, { message: 'Column not found' });
        return;
      }
      
      // Update
      col.priority = req.body.priority || col.priority;
      col.templatePrvlg = req.body.templatePrvlg || col.templatePrvlg;
      col.mtgLoc = req.body.mtgLoc || col.mtgLoc;
      col.locOrder = req.body.locOrder || col.locOrder;
      
      col.save(function(err, updatedCol) {
        
        if (err) {
          sendJsonResponse(res, 400, err);
          return;
        }
        
        sendJsonResponse(res, 201, updatedCol);
      });
      
    })
    
    
  },
  
  
  
  rotationalColsDeleteOneById: function(req, res) {
    // Error-trap 1: Check for column id  
    if (!req.params.colid) {
      sendJsonResponse(res, 400, { message: 'Missing column id' });
      return;
    }
    
    RotationalColModel.findByIdAndRemove(req.params.colid, function(err, col) {
      // Error-trap 2: Catch Mongoose/Mongo errors
      if (err) {
        sendJsonResponse(res, 400, err);
        return;
      }
      
      sendJsonResponse(res, 204, null);
      
    });
    
  }
  
  
}



/* Utility Functions */

function sendJsonResponse(res, status, content) {
  res.status(status);
  res.json(content);
}