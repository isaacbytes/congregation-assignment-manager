// 'api/controllers/rotationalData'

var RotationalDataModel = require('../models/rotationalData');
var moment = require('moment');
var underscore = require('underscore');
var validators = require('../libs/validators');

// abstracts appropriate res.status() and res.json() into one call
function sendJsonResponse(res, status, content) {  
  res.status(status);
  res.json(content);
}




module.exports = {
  
  // get rotational data for one week
  rotationalDataGetWk: function(req, res) {
    // vars
    var momentDate;
    
    // Error trap 1: Check for required params
    if (!req.params.date) {
      sendJsonResponse(res, 400, {message: 'Missing date param in body'});
      return;
    }
    
    // Error trap 2: Validate date param
    if (!validators.valDate(req.params.date, 'MM-DD-YYYY')) {
      sendJsonResponse(res, 400, {message: 'Invalid date. Please use dates in format "MM-DD-YYYY"'});
      return;
    }
    
    // transform to Monday
    momentDate = moment(req.params.date, 'MM-DD-YYYY').startOf('day');
    momentDate = validators.toMonday(momentDate);
    
    RotationalDataModel.findOne(validators.getMongooseQDateObj(momentDate), function(err, rotData) {
      
      // Error trap 3: Check for empty response from Mongoose
      if (!rotData) {
        sendJsonResponse(res, 404, {message: 'No data for specified period.'});
        return;
      }
      
      sendJsonResponse(res, 200, rotData);
    });
    

  },
  
  // get rotational data for month(s)
  rotationalDataGetMonths: function(req, res) {
    var momentDate;
    
    // Error trap 1: Check for required params
    if (!req.params.date) {
      sendJsonResponse(res, 400, {message: 'Missing date param in body'});
      return;
    }
    
    // Error trap 2: Validate date param
    if (!validators.valDate(req.params.date, 'MM-YYYY')) {
      sendJsonResponse(res, 400, {message: 'Invalid date. Please use dates in format "MM-YYYY"'});
      return;
    }
    
    // Create moment
    momentDate = moment(req.params.date, 'MM-YYYY');
    
    
    RotationalDataModel.find(
      validators.getMongooseQDateObj(momentDate, 'month'), 
      null, 
      { sort: 'date'}, 
      function(err, rotData) {
      
        // Error trap 3: Check for empty response from Mongoose
        if (!rotData || rotData.length < 1) {
          sendJsonResponse(res, 404, {message: 'No data for specified period.'});
          return;
        }
        
        // success, send results
        sendJsonResponse(res, 200, rotData);
        }
    );
    
  },
  
  
  // create new rotational data for given month(s)
  rotationalDataCreateMonths: function(req, res) {
    var qrStartDate, qrEndDate, i, curDate, curWkData, 
        qrMonths, qrWeeks, curMondays;
    
    // Error trap 1: check for start/end dates in query strings
    if (!req.query.f) {
      sendJsonResponse(res, 400, {message: 'Missing start query string: \'f\', the end date (optional) is: \'t\'.'});
      return
    }
    
    // Error trap 2: Validate start date
    if (!validators.valDate(req.query.f, 'MM-YYYY')) {
      sendJsonResponse(res, 400, {message: 'Invalid date. Please use dates in format "MM-YYYY"'});
      return;
    }
    
    // If, end date supplied, validate as well
    if (req.query.t) {
      if (!validators.valDate(req.query.t, 'MM-YYYY')) {
        sendJsonResponse(res, 400, {message: 'Invalid date. Please use dates in format "MM-YYYY"'});
        return;
      }
    }
    
    // Error trap 3: Check for req.body (should be an array) 
    if (!Array.isArray(req.body)) {
      sendJsonResponse(res, 400, { message: 'Request body must be an array' });
      return;
    }
    
    /* 01-2016, 03-2016 */
    
    
    // Create moments from query strings
    qrStartDate = moment(req.query.f, 'MM-YYYY').startOf('day');
    qrEndDate = req.query.t ? moment(req.query.t, 'MM-YYYY').startOf('day') : qrStartDate.clone();
    curDate = qrStartDate.clone(); // for loop 
    qrMonths = [],
    qrWeeks = [];
    
    
    while (curDate.isSameOrBefore(qrEndDate, 'month')) {
      // push month to months array
      qrMonths.push(curDate.clone().startOf('month'));
      
      // get mondays in current month
      curMondays = validators.getMondaysByMonth(curDate);
      
      // push mondays of month to weeks array
      Array.prototype.push.apply(qrWeeks, curMondays);
      
      // ++ step ++ //
      curDate.add(1, 'months');
    }
    
    
    // Error trap 4: Check for array length in body
    if (req.body.length !== qrWeeks.length) {
      sendJsonResponse(res, 400, 
        { message: 'No. of elems in array is ' + (req.body.length > qrWeeks.length ? 'greater' : 'fewer') + ' than Mondays in month(s)'});
      return;
    }
    
    
    // Error trap 5: Validate dates in array
    // loop through array and parse date property and convert to Moment
    for (i = 0; i < req.body.length; i++) {
      
      // check each date is present and valid
      if (!req.body[i].date || !validators.valDate(req.body[i].date, 'MM-DD-YYYY moment.ISO_8601')) {
        sendJsonResponse(res, 400, {
          message: 'Invalid/missing date format at array elem ' +i+ '. Format must be \'MM-DD-YYYY\' or ISO-8601'
        });

        // error parsing date, exit function
        return;
      }
      
      req.body[i].date = moment(req.body[i].date, ['MM-DD-YYYY', moment.ISO_8601]);
      
      // convert date to JS Date for DB insertion
      req.body[i].date = validators.toMonday(req.body[i].date).toDate();
    }
    
    
    // Run Mongoose validation
    for (i = 0; i < req.body.length; i++) {
      var curData = new RotationalDataModel(req.body[i]);
      var error = curData.validateSync();
      
      if (error) {
        console.log('Validation failed at index ' + i);
        sendJsonResponse(res, 400, { message: 'Error. Validation halted at index ' + i + '. See Error for more details.', error: error });
        return;
      } 
      
      if (i === req.body.length - 1) {
        console.log('Validation passed!');
      } 
    }
    
    
    // Validation passed, insert!  
    
  
    
    // Still need to check to make sure EACH required date is present
    
    sendJsonResponse(res, 200, { message: 'So far so good!',
                                 qrMonths: qrMonths,
                                 qrWeeks: qrWeeks
                               });
    
    
    // ** check dates in body array, make sure that they match/are complete
    
    // ** Pass each one into validator
    
    // ** Insert into DB
    
     
    
  },
  
  // update rotational data for given month
  rotationalDataUpdateMonths: function(req, res) {
   
    
    
  }
  
};