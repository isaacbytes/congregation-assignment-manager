var ServiceGroup = require('../models/serviceGroup.js');
var _            = require('underscore');


// sets res.status and res.json in one call
function sendJsonResponse(res, status, content) {
  res.status(status);
  res.json(content);
}


module.exports = {
  
  /* ********** GET ALL SERVICE GROUPS ********** */
  serviceGroupsGetAll : function(req, res) {
    
    ServiceGroup.find()
    .populate('overseer', '_id firstName lastName role')
    .populate('aux', '_id firstName lastName role')
    .populate('assignees', '_id firstName lastName role')
    .exec(function(err, serviceGroups) {

      // Error trap 1: Catch Mongoose/DB errors
      if (err) {
        sendJsonResponse(res, 500, {message: 'Seems to be a problem fulfulling your request. Please try again.'});
        return;
      }

      // Error trap 2: Check for empty response from Mongoose
      if (!serviceGroups || serviceGroups.length < 1) {
        sendJsonResponse(res, 404, {message: 'No service groups found'});
        return;
      }

      // No errors, send results
      sendJsonResponse(res, 200, serviceGroups);  

    });
  },
  
  
  
  /* ********** GET ONE SERVICE GROUP BY ID ********** */
  serviceGroupsGetOneById: function(req, res) {
    
    if (!req.params.id) {
      sendJsonResponse(res, 400, {message: 'Missing service group id.'});
      return;
    }
    
    ServiceGroup.findById(req.params.id)
    .populate('overseer', '_id firstName lastName role')
    .populate('aux', '_id firstName lastName role')
    .populate('assignees', '_id firstName lastName role')
    .exec(function(err, serviceGroup) {
      if (err) {
        sendJsonResponse(res, 500, {message: 'Seems to be a problem fulfilling your request. Please try again.'});
        return;
      }
      
      if (!serviceGroup) {
        sendJsonResponse(res, 404, {message: 'No service group found with that id'});
        return;
      }
      
      sendJsonResponse(res, 200, serviceGroup);
      return;
    });
    
  },
  
  
  
  /* ********** CREATE A NEW SERVICE GROUP ********** */
  serviceGroupsCreateOne : function(req, res) {
    
    
    var newServiceGroup = new ServiceGroup(req.body);
    var validationErrors;
    var takenIds,
        idAlreadyExists;
    
    
    // Check pre-existing groups
    ServiceGroup.find({}, '_id', function(err, ids) {
      if (err) {
        sendJsonResponse(res, 500, {message: 'There seems to be a problem on our end. Please try again.', error: err});
        return;
      }
      
      // mutate into array [1,2,3] of taken ids
      takenIds = _.pluck(ids, '_id');
      
      // parseInt to coerce string from JSON _id to number
      idAlreadyExists = _.contains(takenIds, parseInt(req.body._id));
      
      // Error-trap 1: Check for duplicate ids
      if (idAlreadyExists) {
        sendJsonResponse(res, 400, {message: 'There is already a group with id ' + req.body._id});
        return;
      }
      
    
      // Error-trap 2: Validate data!
      validationErrors = newServiceGroup.validateSync();
      
      if (validationErrors) {
        sendJsonResponse(res, 400, {message: 'Validation errors! Please see \'error\' for more information', error: validationErrors});
        return;
      }
      
      
      // Data valid, save via Mongoose!
      newServiceGroup.save(function(err, serviceGrp) {

        // Error-trap 3: Catch Mongoose error
        if (err) {
          sendJsonResponse(res, 500, {message: 'Seems to be a problem on our end. Please try again later.', error: err, takenIds: takenIds});
          return;
        }
        
        
        // populate fields!!
        serviceGrp
          .populate('overseer',   '_id firstName lastName role')
          .populate('aux',        '_id firstName lastName role')
          .populate('assignees',  '_id firstName lastName role', function(err, group) {
         
          
            // catch populate error
            if (err) {
              sendJsonResponse(res, 500, {message: 'Seems to be a problem on our end. Please try again later.', error: err, takenIds: takenIds});
              return;
            }
          
            
            // send populated results back
            sendJsonResponse(res, 201, serviceGrp);
            return;
        });
        
      });  
      
    });
  },
  
  
  
  /* ********** UPDATE A SERVICE GROUP BY ID ********** */
  serviceGroupsUpdateOneById : function(req, res) { 
    
    var updatedServiceGroup,
        validationErrors,
        originalServiceGroup;
    
    // Error trap 1: Check for service group id in params
    if (!req.params.id) {
      sendJsonResponse(res, 400, {message: 'Missing service group id in url.'});
      return;
    } 
    
    
    // Grab group from DB
    ServiceGroup.findById(req.params.id, function(err, group) {
      
      if (err) {
        sendJsonResponse(res, 500, {message: 'Seems to be an error on our end. Please try again.', error: err});
        return;
      }
      
      // update fields, except for '_id'
      if (req.body._id && parseInt(req.body._id) !== group._id) {
        sendJsonResponse(res, 400, {message: 'Cannot update group id. Can only delete/create groups and update its constituents.'});
        return;
      }
      
      
      _.extend(group, req.body);
      
      
      
      // Update!
      group.save(function(err, group) {
        
        // Catch validation errors, or DB errors
        if (err) {
          sendJsonResponse(res, 400, {message:'There seems to be a problem.', error: err});
          return;
        }
        
        

        
        // Populate!
        group
          .populate('overseer',   '_id firstName lastName role')
          .populate('aux',        '_id firstName lastName role')
          .populate('assignees',  '_id firstName lastName role', function(err, group) {
          
          
          // catch population errors
          if (err) {
            sendJsonResponse(res, 500, {message: 'There seems to be a problem.', error: err});
            return;
          }
          
          sendJsonResponse(res, 200, group); 
          
        })
        
      });
      
    });
    
  },
  
  
  
  /* ********** DELETE A SERVICE GROUP BY ID ********** */
  serviceGroupsDeleteOneById : function(req, res) {
    
    // Error-trap: Check for service group id in params
    if (!req.params.id) {
      sendJsonResponse(res, 400, {message: 'Missing service group id in url.'});
      return;
    }
    
    // Perform delete operation
    ServiceGroup.findOneAndRemove({_id : req.params.id}, function(err, deletedServiceGroup) {
      
      // Error trap 2: Catch Mongoose/DB error
      if (err) {
        sendJsonResponse(res, 400, err);
        return;
      }
      
      // Success
      sendJsonResponse(res, 204, null);
      return;
    });
  }
  
  
};