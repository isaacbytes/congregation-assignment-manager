// AUTHENTICATION API controller

var ACCESS_TOKEN_SECRET       = process.env.JWT_SECRET;
var ACCESS_TOKEN_SIGNOPTIONS  = { algorithm:'HS256', expiresIn: '1m' };
var USER_MAX_TOKENS           = 5;

var jwt       = require('jsonwebtoken');
var _         = require('underscore');
var User      = require('../models/user.js');

// abstracts appropriate res.status() and res.json() into one call
function sendJsonResponse(res, status, content) {  
  res.status(status);
  res.json(content);
}



module.exports = {
  
  // 1. receive valid access token, grants a renewed token
  // 2. receive username/password, grants access token
  checkCredentials : function(req, res) {
    
    var authHeader          = req.get('Authorization') || '';
    var token               = authHeader.split(/\s+/).pop() || '';
    var containsUserAndPass = req.body.email && req.body.password;
     
    // 1. Renewing access token
    if (token) {
      
      // verify signature
      jwt.verify(token, ACCESS_TOKEN_SECRET, function(err, oldToken) {
        
        // TOKEN IS JWT-INVALID (expiration, signature, aud, etc)
        if (err) {
          sendJsonResponse(res, 401, err);
          return;
        }
        
        // CHECK JWT BLACKLIST
        User.findById(oldToken.user_id, function(err, user) {
          // catch errors
          if (err) {
            sendJsonResponse(res, 401, {message: 'There was a problem renewing the token', error: err});
            return;
          }
          
          // catch null response (user not found)
          if (!user) {
            sendJsonResponse(res, 401, {message: 'Token has expired. Please sign in again to request another token. User not found', token: oldToken});
            return;
          }
          
          // ----------------------
          
          // search for provided token in User prof
          var oldTokenIndex = _.findIndex(user.tokens, { tokenStr: token });
          var oldTokenActive,
              newTokenPayload;
          
          // if token not found, DO NOT renew token, require sign in
          if (oldTokenIndex < 0) {
            sendJsonResponse(res, 401, {message: 'Token has expired. Please sign in again to request another token. Token not found in user profile', token: token});
            return;
          }
          
          oldTokenActive = user.tokens[oldTokenIndex].active;
          
          // if token has been blacklisted, or revoked, (or logged out), DO NOT renew token, require sign in
          if (!oldTokenActive) {
            sendJsonResponse(res, 401, {message: 'Token has expired. Please sign in to request another token. Token marked as inactive'});
            return;
          }
          
          // ---------------------
          
          // TOKEN IS VALID, proceed
          // delete old token
          user.tokens.splice(oldTokenIndex, 1);
          
          // generate new token
          newTokenPayload = {
            user_id: user._id,
            email: user.email,
            acctType: user.acctType,
            accessPrvlgs: user.accessPrvlgs
          };
          jwt.sign(newTokenPayload, ACCESS_TOKEN_SECRET, ACCESS_TOKEN_SIGNOPTIONS, function(err, newToken) {
            // catch errors
            if (err) {
              sendJsonResponse(res, 500, {message: 'Something went wrong with token renewal. Please try again. (Error signing new token)', err: err});
              return;
            }
            
            // save token to User profile;
            user.tokens.push({
              platform: 'browser',
              active: true,
              tokenStr: newToken
            });
            
            // save user
            user.save(function(err, user) {
              // catch errors
              if (err) {
                sendJsonResponse(res, 500, {message: 'Something went wrong with token renewal. Please try again. (Error saving new signed token to User profile)', err: err});
                return;
              }
              
              // USER SAVED SUCCESSFULLY! Return JWT
              sendJsonResponse(res, 200, {authToken: newToken});
            });
          });
          
        });
        
        
      });
      
    
    }
    
    
    // 2. Checks email & password, grants access token
    else if (containsUserAndPass) {
      var propEmail     = req.body.email;
      var propPassword  = req.body.password;
      var isValid       = false;
      var tokenDiff;
      
      // Error-trap 1: check for user/pass
      if (!req.body.email || !req.body.password) {
        sendJsonResponse(res, 401, {message: 'Missing email/password.'});
        return;
      }
      
      
      console.log('Right before User.find() operation');
      // Lookup the email first
      User.findOne({email: propEmail}, function(err, user) {

        
        
        // Error-trap 2: catch mongoose find() errors
        if (err) {
          sendJsonResponse(res, 400, err);
          return;
        }
        
        // Error-trap 3: catch null response
        if (!user) {
          sendJsonResponse(res, 401, {message: 'Wrong email/password combination. User not found'});
          console.log('Wrong email/password combination. User not found');
          return;
        }
        
        // Found user, now validate password...
        isValid = user.validatePassword(propPassword);
        
        // If password is invalid, send response
        if (!isValid) {
          sendJsonResponse(res, 401, {message: 'Wrong email/password combination'});
          console.log('Wrong email/password combination. User not found');
          return;
        }
        
        // -- DEBUG
        console.log('Initial \'version\' of user: ' + user.__v);
        
        // Password is valid,
        // generate token
        jwt.sign({
          user_id: user._id,
          email: user.email,
          acctType: user.acctType,
          accessPrvlgs: user.accessPrvlgs
        }, ACCESS_TOKEN_SECRET, ACCESS_TOKEN_SIGNOPTIONS, function(err, newToken) {
          // catch errors
          if (err) {
            sendJsonResponse(res, 500, {message: 'Oops. Something went wrong. Please try again later.', error: err});
            return;
          }
          
          // check if any expired/invalidated tokens can be deleted from user profile
          /* bug fix: array must work backwards because splicing it during loop will change indexes - done*/
          for (var i = user.tokens.length-1; i >= 0; i--) {
            var flaggedForDeletion = false;
            
            // check 'active' field in user token list
            if (!user.tokens[i].active) {
              flaggedForDeletion = true;  
            }
            
            // JWT-verify token
            try {
              jwt.verify(user.tokens[i].tokenStr, ACCESS_TOKEN_SECRET);
            } catch(err) {
              flaggedForDeletion = true;
            }
            
            // delete
            if (flaggedForDeletion) {
              user.tokens.splice(i, 1);
            }
          }
          
          console.log('After for loop \'version\' of user: ' + user.__v);
          
          // check if number of tokens exceeds max tokens, if so delete
          if (user.tokens.length >= USER_MAX_TOKENS) {
//            // if we're at max, delete one (we're about to add one later)
//            if (user.tokens.length - USER_MAX_TOKENS === 0) {
//              tokenDiff = 1
//            }
            
            tokenDiff = user.tokens.length - USER_MAX_TOKENS;
            
            user.tokens.splice(0, tokenDiff+1);
          }
          
          console.log('After max tokens count \'version\' of user: ' + user.__v);
          
          // save token to user profile
          user.tokens.push({ 
            platform: 'browser',
            active: true,
            tokenStr: newToken
          });
          
          console.log('After new token push  \'version\' of user: ' + user.__v);
          
          console.log('About to update user tokens...');
          console.log('Update user: ' + user._id);
          
          User.update({_id: user._id}, {$set: {tokens: user.tokens}}, function(err, rawResponse) {  
            if (err) {
              sendJsonResponse(res, 500, {message: 'Oops. Something went wrong. Please try again later...UPDATE COMMAND', error: err});
              return;
            }
            // return token back to user
            sendJsonResponse(res, 200, {authToken: newToken});
          });
        });       
      });
    }
    
    // missing credentials
    else {
      sendJsonResponse(res, 401, { error: 'Please provide user/password/token'});
      return;
    }
  },
  
  
  logOutToken: function(req, res) {
    // req.authTokenRaw (raw encoded token, verified)
    // req.authTokenUser (copy of Mongo record)
    // req.authTokenDecoded (verified, decoded token)
    
    User.findById(req.authTokenDecoded.user_id, function(err, user) {
      var tokenIndex;
      
      // catch find() error (user or token don't exist, so just return success
      // since this is a logout path)
      if (err || !user) {
        sendJsonResponse(res, 200, {message: 'Token user not found', moreInfo: err });
        return;
      }
      
      // look for token and delete 
      tokenIndex = _.findIndex(user.tokens, { tokenStr: req.authTokenRaw });
      
      // if token not found in profile, exit
      if (tokenIndex < 0) {
        sendJsonResponse(res, 200, {message: 'Token already invalidated', moreInfo: 'Token not in user profile'});
        return;
      }
      
      // check if token already invalidated
      if (!user.tokens[tokenIndex].active) {
        sendJsonResponse(res, 200, {message: 'Token already invalidated', moreInfo: 'Token already marked as invalidated'});
        return;
      }
      
      // all went well, invalidate/remove token
      user.tokens.splice(tokenIndex, 1);
      
      user.save(function(err, user) {
        if (err) {
          sendJsonResponse(res, 500, {message: 'Error invalidating token from user profile. Try again, or token will be valid for next 5 minutes'});
          return;
        }
        
        sendJsonResponse(res, 204);
      });
      
    });
    
  }
  
  
};