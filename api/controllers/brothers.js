// holds controllers for all routes to '/api/brothers' 
var Brother = require('../models/brother');


// abstracts appropriate res.status() and res.json() into one call
function sendJsonResponse(res, status, content) {  
  res.status(status);
  res.json(content);
}
 

module.exports = {
  
  
  brothersGetAll : function(req, res) {
    
    Brother.find( req.query.prvlg ? {prvlgs: req.query.prvlg} : null, null, function(err, brothers) {
      
      // Error trap 1: Check for empty response from Mongoose
      if (!brothers || brothers.length < 1) {
        sendJsonResponse(res, 404, { message: 'No brothers found' });
        return;
        
      // Error trap 2: Check for Mongoose error
      } else if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      
      // no error, return results
      sendJsonResponse(res, 200, brothers);
                                                                                                                   
    });
  },
                 
  
  brothersGetOneById : function(req, res) {
    // Error trap 1: check that brother id exists in request object
    if (req.params && req.params.id) {
      
      Brother.findById(req.params.id, function(err, brother) {
        
        // Error trap 2: check for empty response from Mongoose
        if (!brother) {
          sendJsonResponse(res, 404, { message: 'Brother not found' });
          return;
        
        // Error trap 3: If Mongoose, returns error, forward to client
        } else if (err) {
          sendJsonResponse(res, 404, err);
          return;
        }
        
        // no errors, send response
        sendJsonResponse(res, 200, brother);
      });
      
    } else {
      sendJsonResponse(res, 400, { message: 'Missing brother id' });
    }
  
  },
  
  
  brothersUpdateOneById : function(req, res) {
    
    // Error trap: check for required param
    if (!req.params.id) {
      sendJsonResponse(res, 400, { message: 'Brother id is required' });
      return;
    }
    
    Brother.findById(req.params.id, function(err, brother) {
      
      // Error trap: check for null response
      if(!brother) {
        sendJsonResponse(res, 404, { message: 'No brother found with that id'});
        return;
      }
      
      // Error trap: check for mongoose error
      else if (err) {
        sendJsonResponse(res, 400, { message: 'There was an error retrieving record from the database. Check your request and try again' });
        return;
      }
      
      // Update!
      brother.firstName = req.body.firstName || brother.firstName;
      brother.lastName = req.body.lastName || brother.lastName;
      brother.gender = req.body.gender || brother.gender;
      brother.role = req.body.role || brother.role;
      brother.isPublisher = req.body.isPublisher || brother.isPublisher;
      brother.isBaptized = req.body.isBaptized || brother.isBaptized;
      brother.prvlgs = req.body.prvlgs || brother.prvlgs;
//      brother.address = req.body.address || brother.address;
      
      
      brother.save(function(err, brother) {
        
        if (err) {
          sendJsonResponse(res, 400, { message: 'There seems to be a problem saving record to database. Check your request and try again' });
        }
        
        else {
          sendJsonResponse(res, 201, brother);
        }
        
      });
      
    });
    
  },
  
  
  brothersCreateOne : function(req, res) {
    
    var newBrother = new Brother({
      firstName : req.body.firstName,
      lastName : req.body.lastName,
      gender : req.body.gender,
      role : req.body.role,
      isPublisher : req.body.isPublisher,
      isBaptized : req.body.isBaptized,
      prvlgs : req.body.prvlgs
      
    });
    
    newBrother.save(function(err, brother) {
      
      if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      
      sendJsonResponse(res, 201, brother);
      
    });
    
  },
  
  brothersDeleteOneById : function(req, res) {
    
    // Error-trap : check for brother id in url parameters
    if (!req.params.id) {
      sendJsonResponse(res, 400, { message: 'Missing id in request url' });
      return;
    }
    
    Brother.findByIdAndRemove(req.params.id, function(err, brother) {
      
      // Error-trap : check for err from Mongoose/Mongo
      if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      sendJsonResponse(res, 204, null);
    });
    
  }
  
};