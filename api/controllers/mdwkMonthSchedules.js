var MdwkMtg = require('../models/mdwkMtg');
var moment = require('moment');
var validators = require('../libs/validators');


// abstracts appropriate res.status() and res.json() into one call
function sendJsonResponse(res, status, content) {  
  res.status(status);
  res.json(content);
}


module.exports = {
  
  getMonthData: function(req, res) {
    var momentDate;
    
    // Error trap 1: Check for required date in params
    if (!req.params.date) {
      sendJsonResponse(res, 400, {message: 'Missing date "MM-YYYY" in params'});
      return;
    }
    
    // Error trap 2: Check for valid date format
    if (!validators.valDate(req.params.date, 'MM-YYYY')) {
      sendJsonResponse(res, 400, { message: 'Invalid date format. Please query in the format "MM-YYYY"' });
      return;
    }
    
    // Date is good, instantiate Moment
    momentDate = moment(req.params.date, 'MM-YYYY', true).startOf('month');
    
    
    // QUERY Mongoose
    MdwkMtg.find(validators.getMongooseQDateObj(momentDate, 'month'), req.query.q ? req.query.q : null, { sort: 'date' }, function(err, mdwkMonthSchedule) {
      
      // Error trap 3: Catch Mongoose errors
      if (err) {
        sendJsonResponse(res, 500, err);
        return;
      }
      
      
      /* ********** IF CHECKING FOR isComplete ********** */
      // if checking for isComplete, month, carry out business logic
      if (req.query.q === 'isComplete') {
        var monthMondays = validators.getMondaysByMonth(momentDate);
        
        // 1. First check for empty response (month not even started)
        if (!mdwkMonthSchedule || mdwkMonthSchedule.length < 1) {
          sendJsonResponse(res, 200, { isComplete: false });
          return;
        }
        
        // 2. Check for array length
        if (mdwkMonthSchedule.length < monthMondays.length) {
          sendJsonResponse(res, 200, { isComplete: false });
          return;
        }
        
        // 3. Check individual properties of month
        for (var i = 0; i < mdwkMonthSchedule.length; i++) {
          if (mdwkMonthSchedule[i].isComplete === false) {
            sendJsonResponse(res, 200, { isComplete: false });
            return;
          }
          else if (i === mdwkMonthSchedule.length - 1) {
            sendJsonResponse(res, 200, { isComplete: true });
            return;
          }
        }
      }
      
      /* ********** IF NOT, CONTINUE ERROR-CHECKING ********** */
      // Error trap 4: Check for empty response from Mongoose
      if (!mdwkMonthSchedule || mdwkMonthSchedule.length < 1) {
        sendJsonResponse(res, 404, { message: 'No results found for that month' });
        return;
      }
      
      // if not, send back entire month
      sendJsonResponse(res, 200, mdwkMonthSchedule);
    });

  },
  
  
  // HANDLES BOTH CREATING AND READING FILES
  createOrUpdateMonth: function(req, res) {
    var momentDate,
        monthMondays,
        iterateMoment,
        i,
        returnMonthArr = [],
        mongooseSuccess;
    
    /* **** VALIDATE DATE **** */
    // Error trap 1: Check for missing date
    if (!req.params.date) {
      sendJsonResponse(res, 400, { message: 'Missing date parameter' });
      return;
    }
    
    // Error trap 2: Check for invalid date format
    if (!validators.valDate(req.params.date, 'MM-YYYY')) {
      sendJsonResponse(res, 400, { message: 'Invalid date format. Please query in the format "MM-YYYY"' });
      return;
    }
    
    // Date is good, instantiate Moment
    momentDate = moment(req.params.date, 'MM-YYYY', true).startOf('month');
    
    
    // Error trap 3: Check for req.body (should be an array) 
    if (!Array.isArray(req.body)) {
      sendJsonResponse(res, 400, { message: 'Request body must be an array' });
      return;
    }
    
    // get no. of mondays in month to test array length
    monthMondays = validators.getMondaysByMonth(momentDate.format('MM-YYYY'));
    
    // Error trap 4: Check array length for superfluous/lack of data
    if (req.body.length !== monthMondays.length) {
      sendJsonResponse(res, 400, 
        { message: 'No. of elems in request array is ' + (req.body.length > monthMondays.length? 'greater' : 'smaller') + ' than Mondays in given month'});
      return;
    }
     
    // Error trap 5: validate Dates in array 
    // loop through array and parse date property and convert to Moment
    for (i = 0; i < req.body.length; i++) {

      // check dates are present and of correct format
      if (!req.body[i].date || !validators.valDate(req.body[i].date, 'MM-DD-YYYY moment.ISO_8601')) {
        sendJsonResponse(res, 400, { 
          message: 'Invalid/missing date format at array elem number ' + i + '. Format must be "MM-DD-YYYY" or ISO-8601 format' 
        });
        // error parsing (bad format), exit fn
        return;
      }
      
      req.body[i].date = moment(req.body[i].date, ['MM-DD-YYYY', moment.ISO_8601]);
      
      req.body[i].date = validators.toMonday(req.body[i].date).toDate();
    }
    
    
    // Time to try inserting each one!
    for (i = 0; i < req.body.length; i++) {
      
      var thisMtg = new MdwkMtg(req.body[i]);
      
      var error = thisMtg.validateSync();
      
      if (error) {
        sendJsonResponse(res, 400, { message: 'Error. Validation halted at index ' + i + '. See Error for more details.', error: error });
        return;
      } 
      
      // tag with isComplete, if validation passed
      else {
        req.body[i].isComplete = true;
      }
    }
    
    
    
    // Validation passed, insert!
    for (i = 0; i < req.body.length; i++) {
      
      // closure for preserving 'i' state
      (function() {
        var curIndex = i;
        
        MdwkMtg.findOneAndUpdate(
          // Arg 1: Query Object
          validators.getMongooseQDateObj(moment(req.body[curIndex].date)),
          
          // Arg 2: Update Object
          req.body[curIndex],
          
          
          // Arg 3: Options Object
          { upsert: true, new: true, runValidators: false },
          
          // ^ no need to re-run validation. If validation is set here,
          // then the 'this' value is messed up and undefined. Unless you
          // then set the 'context' option to 'query' in the above object.
          
          
          // Arg 4: Callback
          function(err, mtg) {
            
            // Error-trap: catch errors (validation should've been passed already)
            if (err) {
              console.log('There was an unexpected error while updating index number ' + curIndex);
              sendJsonResponse(res, 500, { message: 'There seems to be a problem on our end. Please try again', error: err, index: curIndex });
              return; // return doesn't exit loop, since it is called as a callback function
            }
            
            // Reached last record without error, return message
            if (curIndex === req.body.length - 1) {
              sendJsonResponse(res, 200, { message: 'Success' });
            }
            
          }
          
        
        );
      
      })();
      
      
    }
    
  }
  
};