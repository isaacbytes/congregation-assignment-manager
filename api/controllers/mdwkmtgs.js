// holds controllers for all routes to Midweek Meetings 
var MdwkMtg = require('../models/mdwkMtg');
var moment = require('moment');
var validators = require('../libs/validators');


// abstracts appropriate res.status() and res.json() into one call
function sendJsonResponse(res, status, content) {  
  res.status(status);
  res.json(content);
}


module.exports = {
   
  
  mdwkMtgsGetAll: function(req, res) {
    MdwkMtg.find(function(err, mtgs) {
      // Error trap 1: check for empty response from Mongoose
      if (mtgs.length < 1) {
        sendJsonResponse(res, 404, { message: 'No midweek meetings found.' });
        return;
      }
      
      // Error trap 2: check for Mongo error
      else if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      sendJsonResponse(res, 200, mtgs);
    });
  },
  
  
  mdwkMtgsGetOneById: function(req, res) {
    // Error trap 1: check for required mdwk mtg id in url
    if (!req.params.id) {
      sendJsonResponse(res, 400, { message: 'Missing meeting id.' });
      return;
    }
    
    MdwkMtg.findById(req.params.id, function(err, mtg) {
      // Error trap 2: check for empty response from mongo
      if (!mtg) {
        sendJsonResponse(res, 404, { message: 'No meeting found.' });
        return;
      }
      // Error trap 3: catch mongoose error
      else if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      sendJsonResponse(res, 200, mtg);
    });
  },
  
  
  mdwkMtgsGetOneByDate: function(req, res) {
    var momentDate;
    
    // Error trap 1: check for required date param in url
    if (!req.params.date) {
      sendJsonResponse(res, 404, { message: 'Missing date.' });
      return;
    }
    
    // Error trap 2: Check for valid date format
    if (!validators.valDate(req.params.date, 'YYYY-MM-DD moment.ISO_8601')) {
      sendJsonResponse(res, 400, { message: 'Invalid date.' });
      return;
    }
    
    // instantiate new Moment
    momentDate = moment(req.params.date).startOf('day');
    
    // Transform Moment to Monday for Mongoose lookup
    momentDate = validators.toMonday(momentDate);
    
    // QUERY
    MdwkMtg.findOne(validators.getMongooseQDateObj(momentDate), req.query.q?req.query.q:null, function(err, mtg) {
      
      // Error trap 3: Check for empty response from Mongoose
      if (!mtg) {
        sendJsonResponse(res, 404, { message: 'No meetings found.' });
        return;
      }
      sendJsonResponse(res, 200, mtg);      
    });
  },
  
  
  mdwkMtgsCreateOneByDate: function(req, res) {
    var momentDate;
    
    // Error trap 1: check for required date param in url
    if (!req.params.date) {
      sendJsonResponse(res, 404, { message: 'Missing date.' });
      return;
    }
    
    // Error trap 2: Check for valid date format 
    if (!validators.valDate(req.params.date, 'YYYY-MM-DD moment.ISO_8601')) {
      sendJsonResponse(res, 400, { message: 'Invalid date.' });
      return;
    }
    
    // instantiate new Moment, transform to Monday
    momentDate = validators.toMonday(moment(req.params.date).startOf('day'));
    
    // FIRST LOOKUP - Check for pre-existing meeting data
    MdwkMtg.findOne(validators.getMongooseQDateObj(momentDate), null, function(err, mtg) {
      
      // Error trap 3: Mongoose error
      if (err) {
        sendJsonResponse(res, 500, { message: 'Oops. Error saving to the database', dbErr: err });
        return;
      }
      
      // Error trap 4: Check for any data for given date
      if (mtg) {
        sendJsonResponse(res, 400, { message: 'Oops. Meeting already exists for this date.' });
        return;
      }
      
      // PASSED - proceed with POST request
      // delegate to separate function?
      MdwkMtg.create({
        
        date:                     momentDate.toDate(),
        specialWk:                req.body.specialWk,
        specialWkMsg:             req.body.specialWkMsg, 
        chairman:                 req.body.chairman,
        chairmanID:               req.body.chairmanID,
        weeklyBibReading:         req.body.weeklyBibReading,
        introSong:                req.body.introSong,
        midSong:                  req.body.midSong,
        concSong:                 req.body.concSong,
        concPrayer:               req.body.concPrayer,
        concPrayerID:             req.body.concPrayerID,
        treasuresCollection:      req.body.treasuresCollection,
        applyYourselfCollection:  req.body.applyYourselfCollection,
        livingAsCollection:       req.body.livingAsCollection
        
      }, function(err, createdMtg) {
        
        // Error trap 5: DB Error handling
        if (err) {
          sendJsonResponse(res, 404, { message: 'Oops. Error saving to the database.', dbErr: err });
          return;
        }
        
        // successful save, return createdMtg
        sendJsonResponse(res, 201, createdMtg);
        
      });
      
    });
    
    
  },
  
  
  mdwkMtgsUpdateOneById: function(req, res) {
    // Error trap 1: check for meeting ID parameter
    if (!req.params.id) {
      sendJsonResponse(res, 400, { message: 'Missing meeting id.' });
      return;
    }
    
    MdwkMtg.findById(req.params.id, function(err, mtg) {
      // Error trap 2: trap Mongoose lookup failure
      if (err) {
        sendJsonResponse(res, 404, err);
        return;
      }
      
      mtg.specialWk =               req.body.specialWk        || mtg.specialWk;
      mtg.specialWkMsg =            req.body.specialWkMsg     || mtg.specialWkMsg;
      mtg.chairman =                req.body.chairman         || mtg.chairman;
      mtg.chairmanID =              req.body.chairmanID       || mtg.chairmanID;
      mtg.weeklyBibReading =        req.body.weeklyBibReading || mtg.weeklyBibReading;
      mtg.introSong =               req.body.introSong        || mtg.introSong;
      mtg.midSong =                 req.body.midSong          || mtg.midSong;
      mtg.concSong =                req.body.concSong         || mtg.concSong;
      mtg.concPrayer =              req.body.concPrayer       || mtg.concPrayer;
      mtg.concPrayerID =            req.body.concPrayerID     || mtg.concPrayerID;
      mtg.treasuresCollection =     req.body.treasuresCollection ||          mtg.treasuresCollection;
      mtg.applyYourselfCollection = req.body.applyYourselfCollection ||          mtg.applyYourselfCollection;
      mtg.livingAsCollection =      req.body.livingAsCollection ||           mtg.livingAsCollection;
      
      
      mtg.save(function(err, savedMtg) {
        // Error trap 3: trap Mongoose save failure
        if (err) {
          sendJsonResponse(res, 400, err);
          return;
        }
        
        // success
        sendJsonResponse(res, 200, savedMtg);
      });
    });
    
    
  },
  
  
  mdwkMtgsDeleteOneById: function(req, res) {
    // Error trap 1: check for meeting ID to delete
    if (!req.params.id) {
      sendJsonResponse(res, 400, { message: 'Missing meeting id.' });
      return;
    }
    
    MdwkMtg.findByIdAndRemove(req.params.id, function(err, mtg) {
      // Error trap 2: catch errors from Mongoose (validation, Mongo err, etc)
      if (err) {
        sendJsonResponse(res, 404, { message: 'Oops. Error saving to the database. Check for errors.', dbErr: err });
        return;
      }
      // success
      sendJsonResponse(res, 204, null);
    });
    
    
  },
  
  
  dateResource: function(req, res) {
    if (!req.params.date) {
      sendJsonResponse(res, 400, {message:'Missing'});
      return;
    }
    
    
    if (!validators.valDate(req.params.date, 'MM-YYYY')) {
      sendJsonResponse(res, 400, {message: 'Invalid format'});
      return;
    }
    
    var mondays = validators.getMondaysByMonth(req.params.date);
    var output = [];
    
    for (var i = 0; i < mondays.length; i++) {
      output.push(mondays[i].format('MM-DD-YYYY'));
    }
    
    
    sendJsonResponse(res, 200, output);
    
  }
  
  
};
