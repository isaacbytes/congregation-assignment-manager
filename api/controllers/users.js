// USER REGISTRATION controller -- ** protected ** --

// [GET] [POST] [DELETE] [PUT] user registration/editing


var User = require('../models/user');
var validators = require('../libs/validators');

// abstracts appropriate res.status() and res.json() into one call
function sendJsonResponse(res, status, content) {  
  res.status(status);
  res.json(content);
}



module.exports = {
  
  // *** [GET] all registered users
  getAllUsers : function(req, res) {
    
    User.find(function(err, users) {
    
      // Error-trap 1: catch mongoose find() errors
      if (err) {
        sendJsonResponse(res, 400, err);
        return;
      }
      
      sendJsonResponse(res, 200, users);
      
    });
    
  },
  
  
  // *** [GET] a single user by user id
  getOneUser : function(req, res) {
    // Error-trap 1: Check for required params
    if (!req.params.id) {
      sendJsonResponse(res, 400, {message: 'Please provide a user ID'});
      return;
    }
    
    User.findById(req.params.id, function(err, user) {
      
      // Error-trap 2: Catch mongoose find() errors
      if (err) {
        sendJsonResponse(res, 400, {message: 'Error looking for user. Make sure user ID is correct', error: err});
        return;
      }
      
      // Error-trap 3: Check for null/empty response
      if (!user) {
        sendJsonResponse(res, 404, {message: 'User not found'});
        return;
      }
      
      sendJsonResponse(res, 200, user);
    });
  },
  
  
  // *** [POST] a single user (user registration)
  createUser : function(req, res) {
    // Error-trap 1: Check for required params (in body)
    if (!req.body.email || !req.body.password || !req.body.acctType || !req.body.firstName || !req.body.lastName) {
      sendJsonResponse(res, 400, {message: 'Missing required information.'});
      return;
    }
    
    /* VALIDATE!!  Validate username and password */
    
    // Validation passed, proceed
    var newUser = new User({firstName: req.body.firstName, lastName: req.body.lastName, email: req.body.email, acctType: req.body.acctType});
    
    // CRYPTO - hash password!
    newUser.setPassword(req.body.password);
    
    // save user
    newUser.save(function(err, user) {
      // Error-trap 2: Catch Mongoose save() errors
      if (err) {
        sendJsonResponse(res, 400, {message: 'Error saving user.', error: err});
        return;
      }
      
      // return newly created user
      sendJsonResponse(res, 201, user);
    });
  },
  
  
  
  // *** [PUT] edit a single user
  editUser : function(req, res) {
    // Error trap 1: Check for required params.
    if (!req.params.id) {
      sendJsonResponse(res, 400, {message: 'Missing user id.'});
      return;
    }
    
    // Grab user
    var editedUser;
    var request = req.params;
    
    User.findById(req.params.id, function(err, user) {
      // Error-trap 2: Catch Mongoose find() errors
      if (err) {
        sendJsonResponse(res, 400, {message: 'Error finding user.', error: err});
        return;
      }
      
      // Error-trap 3: Check for null response (404)
      if (!user) {
        sendJsonResponse(res, 400, {message: 'User not found.', error: err});
        return;
      }
      
      // success, grab user and start editing
      editedUser = user;
      
      // go through req.params to find edits, and make appropriate modifications
      editedUser.email = request.email || editedUser.email;
      
      // if changed, hash new password 
      if (request.password) {
        editedUser.setPassword(request.password);
      }
      
      // save new user details
      editedUser.save(function(err, user) {
        
        // Error-trap 4: Catch Mongoose save() errors
        if (err) {
          sendJsonResponse(res, 400, {message: 'Error updating user.', error: err});
          return;
        }
        
        // Success. Return updated user
        sendJsonResponse(res, 200, user);
      });
    }); 
    
    
  },
  
  
  // *** [DELETE] a single user
  deleteUser : function(req, res) {
    // Error-trap 1: Check  for required params
    if (!req.params.id) {
      sendJsonResponse(res, 400, {message: 'Missing user id.'});
      return;
    }
    
    // Find and delete user from DB
    User.findByIdAndRemove(req.params.id, function(err, user) {
      
      // Error-trap 2: Catch Mongoose find() / remove() errors
      if (err) {
        sendJsonResponse(res, 400, {message: 'Error removing user.', error: err});
        return;
      }
      
      // user not found
      if (!user) {
        sendJsonResponse(res, 404, {message: 'User not found.'});
        return;
      }
      sendJsonResponse(res, 200, {message: 'Success'});  
    });
    
  }
  
  
}