// server.js


// modules =================================================
require('dotenv').load();
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var jwt = require('jsonwebtoken');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer');
var methodOverride = require('method-override');
var underscore = require('underscore');
require('./config/db.js'); // connect to DB and initialize settings

// configuration ===========================================

// set PORT
var port = process.env.PORT || 8080;

// grab JWT Secret file 
var ACCESS_TOKEN_SECRET = process.env.JWT_SECRET;

// require User model to configure Passport HTTP Bearer Strategy
var User = require('./api/controllers/users');
var validators = require('./api/libs/validators');
    
// get data from request body
app.use(bodyParser.json());

// parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(methodOverride('X-HTTP-Method-Override'));

// Register Passport Strategy: Passport-Bearer-Strategy
//passport.use(new BearerStrategy(validators.bearerStrategyConfigFn));

// serve static files from 'public' folder (use absolute path of directory - safer)
app.use(express.static(__dirname + '/public'));


// use morgan to log requests
app.use(morgan('dev'));


// routes ==================================================
require('./api/routes/index')(app);


// start app ===============================================
// start at http://localhost:8080
app.listen(port);

// shoutout to the user
console.log('Magic happens on port ' + port);

// expose app
exports = module.exports = app;
